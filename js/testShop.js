angular
  .module('TestShop', [
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ui.bootstrap',
    'LocalStorageModule',
    'ngTouch'
  ])
  .constant('API_Configs', {
    'location': 'http://localhost/api/v1',
    'secret': 'ed97c863c1b1f69b2109b41c870db0bd',
    'client_id': '3690dd5f6d552f2d75f0f40c55099313',

  })
  .config(function ($routeProvider) {
    for( var i in routes) {
      var route = routes[i];
      $routeProvider.when(route.route, route)
    }
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });
  })

  .factory('authHttp',['$q', '$location', '$rootScope', 'localStorageService', 'API_Configs', function($q, $location, $rootScope, localStorageService, API_Configs){
  	return {
  		request: function(config){
  		  if(config.url.indexOf(API_Configs.location) >=0 ){
  		    if(config.url != API_Configs.location+"/auth/token") {
  		      var token = localStorageService.get("TestShopToken");
      			if(token){
      				config.headers.Authorization = 'Bearer ' + token;
      			} else {
      				localStorageService.remove('TestShopToken');
      				//$rootScope.$broadcast("NOTIFICATION", {type: 'danger', msg: "Access Denied"});
      			}
  		    }

  		  }
  			return config || $q.when(config);
  		},
  	}
  }])
  .factory('User', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/me", {}, {
      me: {
        method: 'GET',
        isArray: false
      },
      create: {
        method: 'POST',
        url: API_Configs.location + "/users",
        isArray: false
      },
      orders: {
        method: 'GET',
        url: API_Configs.location + "/me/orders",
        isArray: false
      },
      showOrders: {
        method: 'GET',
        url: API_Configs.location + "/me/orders/:id",
        params: {id: "@id"},
        isArray: false
      },
      createOrder: {
        method: 'POST',
        url: API_Configs.location + "/me/orders",
        isArray: false
      },
      summaryOrder: {
        method: 'POST',
        url: API_Configs.location + "/me/orders/summary",
        isArray: false
      },
      addresses: {
        method: 'GET',
        url: API_Configs.location + "/me/addresses",
        isArray: false
      },
      createAddress: {
        method: 'POST',
        url: API_Configs.location + "/me/addresses",
        isArray: false
      },
    }))
  })
  .factory('Auth', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/oauth/token", {}, {
      token: {
        method: 'POST',
        isArray: false
      }
    }))
  })
  .factory('Products', function ($resource, API_Configs) {
    return ($resource( API_Configs.location + "/products/:id", {id: "@id"}, {
      index: {
        method: 'GET',
        url: API_Configs.location + "/products/",
        isArray: false
      },
      show: {
        method: 'GET',
        isArray: false
      },
    }))
  })
  .factory('Coupons', function ($resource, API_Configs){
    return ($resource( API_Configs.location + "/coupons", {}, {
      check: {
        method: 'POST',
        isArray: false
      }
    }))
  })
  .run(['localStorageService', '$rootScope', 'User', function(localStorageService, $rootScope, User){
    $rootScope.$on("APPLY_DISCOUNT", function (event, message){
      localStorageService.set("TestShopCoupon", message);
      $rootScope.$broadcast("HAS_COUPON", message);
    })

    $rootScope.$on("PRODUCT_ADD", function(event, $message){
        var added = false;
        $rootScope.cart.map(function (item){
          if(item.ProductId == $message.ProductId){
            item.Quantity += $message.Quantity;
            added = true;
          }
        })
        if(added == false){
          $rootScope.cart.push($message);
        }
        localStorageService.set("TestShopCart", $rootScope.cart);
    })
    $rootScope.$on("PRODUCT_CHANGED", function (event, message){
      $rootScope.cart.map(function(item){
        if(item.ProductId == message.ProductId){
          item.Quantity = message.Quantity;
        }
      })
      localStorageService.set("TestShopCart", $rootScope.cart);
    })
    $rootScope.$on("CLEAN_CART", function (event, $message){
      $rootScope.cart = [];
      $rootScope.coupon = null;
      localStorageService.set("TestShopCart", $rootScope.cart);
      localStorageService.set("TestShopCoupon", $rootScope.coupon);
    })
    $rootScope.$on("PRODUCT_REMOVE", function(event, $message){
        $rootScope.cart = $rootScope.cart.filter(function ($item){
          return $item.ProductId != $message.ProductId;
        });
        localStorageService.set("TestShopCart", $rootScope.cart);
    })
    $rootScope.$on("USER_LOGGED", function (event){
        if(localStorageService.get("TestShopToken")){
          User.me().$promise.then(function (data){
            $rootScope.userData = data;
            localStorageService.set("TestShopUser", data);
          })
        }
    })

    if(!localStorageService.get("TestShopCart")){
      localStorageService.set("TestShopCart", []);
      $rootScope.cart = [];
    } else {
      $rootScope.cart = localStorageService.get("TestShopCart");
    }
    if(!localStorageService.get("TestShopCoupon")){
      //localStorageService.set("TestShopCoupon", null);
      $rootScope.coupon = null;
    } else {
      $rootScope.coupon = localStorageService.get("TestShopCoupon");
      $rootScope.$broadcast("HAS_COUPON", $rootScope.coupon);
    }
    $rootScope.userData = null;

    if(localStorageService.get("TestShopToken")){
      if(!localStorageService.get("TestShopUser")){
        $rootScope.$broadcast("USER_LOGGED", {});
      } else {
        $rootScope.userData = localStorageService.get("TestShopUser");
      }
    }



    $rootScope.$off = function (name, listener){
      var namedListeners = this.$$listeners[name];
      if(namedListeners){
        for(var i = 0; i < namedListeners.length; i++){
          if(namedListeners[i] === listener){
            return namedListeners.splice(i, 1);
          }
        }
      }
    }
  }])
  .config(['$httpProvider',function($httpProvider) {
  	//$httpProvider.interceptors.push('authHttpFailure');
  	$httpProvider.interceptors.push('authHttp');
  	$httpProvider.defaults.headers.common.Authorization = 'Bearer ';
  }])
  .directive("currentCart", ['$rootScope', '$uibModal', function ($rootScope, $uibModal) {
    return {
      restrict: 'E',
      link: function(scope, element, attrs){

        scope.alreadyOpen = false;
        $rootScope.$on("PRODUCT_ADD", function (event, message){
          // Modal
          //console.log(value);
          if(message){
            if(!scope.alreadyOpen) {
              var modal = $uibModal.open({
                templateUrl: 'views/cart.html',
                controller: 'CartCtrlModal',
                backdrop: 'static'
              })
              scope.alreadyOpen = true;
              modal.result.then(function (){
                console.log("RODOU");
                scope.alreadyOpen = false;
              })
            }
          }
        })

      }
    }
  }])
  .controller("LogoutCtrl", ['$scope', '$rootScope', 'localStorageService', '$location', function ($scope, $rootScope, localStorageService, $location){
    localStorageService.set("TestShopToken", null);
    localStorageService.set("TestShopUser", null);

    delete $rootScope.userData;

    $location.path("/");

  }])
  .controller("SignupCtrl", ['$scope', '$rootScope', 'localStorageService', '$location', 'User', 'Auth', 'API_Configs', function ($scope, $rootScope, localStorageService, $location, User, Auth, API_Configs){
    //localStorageService.set("TestShopToken", null);
    //localStorageService.set("TestShopUser", null);

    //delete $rootScope.userData;

    //$location.path("/");
    $scope.signErrors = null;

    $scope.create = function (){
      if($scope.signErrors){
        $scope.signErrors = null;
      }

      function logUser(){
        return Auth.token({}, {
          appSecret: API_Configs.secret,
          appKey: API_Configs.client_id,
          email: $scope.userEmail,
          pass: $scope.userPass
        }).$promise.then(function (data){
          if(data.token){
            localStorageService.set("TestShopToken", data.token);
            $rootScope.$broadcast("USER_LOGGED", {});
            $scope.userLogged = true;
            $location.path("/checkout");
          }
        })
      }
      User.create({}, {
        name: $scope.userName,
        email: $scope.userEmail,
        pass: $scope.userPass
      }).$promise.then(function (data){
          logUser();
      }).catch(function (e){
        if(e.message){

          $scope.signErrors = e.message;
        }
      }).finally(function (e){
        if(e.message){

          $scope.signErrors = e.message;
        }
      })
    }
  }])
  .controller("LoginCtrl", ['$scope', '$rootScope', 'localStorageService', 'User', 'Auth', 'API_Configs', '$location', function ($scope, $rootScope, localStorageService, User, Auth, API_Configs, $location){
    $scope.userLogin = function (){
      if($scope.loginMessage){
        delete $scope.loginMessage;
      }
      Auth.token({}, {
        appSecret: API_Configs.secret,
        appKey: API_Configs.client_id,
        email: $scope.userEmail,
        pass: $scope.userPass
      }).$promise.then(function (data){
        if(data.token){
          localStorageService.set("TestShopToken", data.token);
          $rootScope.$broadcast("USER_LOGGED", {});
          $scope.userLogged = true;
          $location.path("/");
        }
      }).catch(function (e){
        $scope.loginMessage = "User or password invalid";
      })

    }
  }])
  .controller("CheckoutCtrl", ['$scope', '$rootScope', 'localStorageService', 'User', 'Auth', 'API_Configs', function ($scope, $rootScope, localStorageService, User, Auth, API_Configs){
      $scope.userLogged = false;
      if(localStorageService.get("TestShopToken")){
        $scope.userLogged = true;
      }

  }])
  .controller("AddressesCtrl", ['$scope', 'User', function ($scope, User){
    $scope.addresses = [];
    User.addresses({}).$promise.then(function (data){
      $scope.addresses = data.content;
    })

    $scope.form_errors = null
    $scope.createAddress = function (){
      $scope.form_errors = null;
      User.createAddress({}, {
        fullName: $scope.fullName,
        addressLine: $scope.addressLine,
        city: $scope.city,
        province: $scope.province,
        country: $scope.country,
        zipCode: $scope.zipCode,
        telephone: $scope.telephone
      }).$promise.then(function (e){
        User.addresses({}).$promise.then(function (data){
          $scope.addresses = data.content;
          $scope.fullName = "";
          $scope.addressLine = "";
          $scope.city = "";
          $scope.province = "";
          $scope.country = "";
          $scope.zipCode = "";
          $scope.telephone = "";

        })
      }).catch(function (e){
        $scope.form_errors = e.message;
      })
    }
  }])
  .controller("CartCtrl", ['$scope', '$rootScope', 'localStorageService', 'Coupons', 'User', '$location', function ($scope, $rootScope, localStorageService, Coupons, User, $location){
    $scope.coupon = $rootScope.coupon;
    if($scope.coupon){
      $scope.couponCode = $scope.coupon.Code;
    }
    $rootScope.$on("HAS_COUPON", function (event, message){
      $scope.coupon = message;

    })
    if($rootScope.userData){
      $scope.addresses = [];
      User.addresses().$promise.then(function (e){
        $scope.addresses = e.content;
      })
      $scope.sameBilling = true;
    }
    $scope.orderTotal = false;
    $scope.$watch("currentItens", function (values, oldValue){
      if(values){
        //if($scope.orderTotal == false){
        $scope.orderTotal = 0;
        //}
        values.map(function (item){
          $scope.orderTotal += item.Product.Price*item.Quantity;
        })
        if($scope.coupon){
          updateCouponValue($scope.coupon);
        }
      }
    })
    $scope.discount = 0;
    function updateCouponValue(value){
      if(value && value.Discount){
        switch(value.Type){
          case "PERCENT":
            $scope.discount = $scope.orderTotal * (value.Discount/100);
          break;
          case "PRICE":
            $scope.discount = value.Discount;
          break;
        }
      }
    }
    $scope.$watch("coupon", function (value, oldValue){
        updateCouponValue(value);
    })
    $scope.currentItens = localStorageService.get("TestShopCart");


    $scope.updateItemQuantity = function (item){
      $rootScope.$broadcast("PRODUCT_CHANGED", item);
    }
    $scope.removeItem = function (product){
      if(window.confirm("Are you sure?")){
        $rootScope.$broadcast("PRODUCT_REMOVE", product);
        $scope.currentItens = $rootScope.cart;
      }
    }
    $scope.checkCoupon = function(event){
        event.preventDefault();
        Coupons.check({}, {coupon: $scope.couponCode}).$promise.then(function (data){
            if(data.status == "ERROR"){
              window.alert("Invalid Coupon");
              $scope.couponCode = "";
            } else {
              $rootScope.$broadcast("APPLY_DISCOUNT", data);
            }
        }).catch(function (err){
          window.alert("Invalid Coupon");
          $scope.couponCode = "";
        })
    }
    $scope.cleanCart = function (){
      $rootScope.$broadcast("CLEAN_CART", {});
      $scope.coupon = $rootScope.coupon;
      $scope.currentItens = $rootScope.cart;
    }
    $scope.createOrder = function (){
      var req = {};
      req.products = [];
      req.quantities = [];
      if(!$scope.billingAddress){
        return window.alert("Select one Billing Address");
      }
      req.billingId = $scope.billingAddress;

      if($scope.sameBilling){
        req.addressId = req.billingId;
      } else {
        req.addressId = $scope.address;
      }
      if($scope.coupon){
        req.coupon = $scope.coupon.Code;
      }

      $scope.currentItens.map(function (item){
        req.products.push(item.ProductId);
        req.quantities.push(item.Quantity);
      })

      User.createOrder({}, req).$promise.then(function (data){
          $rootScope.$broadcast("CLEAN_CART", {});
          $location.path("/orders/"+data.Id);
      }).catch(function (data){
        window.alert(data.message);
      })



    }

  }])
  .controller("OrdersCtrl", ['$scope', 'User', '$route', function ($scope, User, $route){
    $scope.orders = [];
    $scope.discount = 0;
    function updateCouponValue(value){
      if(value && value.Discount){
        switch(value.Type){
          case "PERCENT":
            $scope.discount = $scope.orderTotal * (value.Discount/100);
          break;
          case "PRICE":
            $scope.discount = value.Discount;
          break;
        }
      }
    }
    function loadOrder(id){
      User.showOrders({id: id}).$promise.then(function (data){
          $scope.selectedOrder = data;
          $scope.orderTotal = $scope.selectedOrder.Total;
          if($scope.selectedOrder.Coupon){
            updateCouponValue($scope.selectedOrder.Coupon);
          }
      })
    }
    User.orders().$promise.then(function (data){
      $scope.orders = data.content;
      if(parseInt($route.current.params.id)){
        loadOrder(parseInt($route.current.params.id));
      } else {
        if($scope.orders.length > 0){
          loadOrder(parseInt($scope.orders[0]['Id']));
        }
      }
    })

  }])
  .controller("CartCtrlModal", ['$location', '$scope', '$rootScope', '$route', 'localStorageService', '$uibModalInstance', function ($location, $scope, $rootScope, $route, localStorageService, $uibModalInstance){
    $scope.currentItens = localStorageService.get("TestShopCart");

    $scope.close = function (){
      $uibModalInstance.close({});
    }
    $scope.removeItem = function (product){
      if(window.confirm("Are you sure?")){
        $rootScope.$broadcast("PRODUCT_REMOVE", product);

        $scope.currentItens = $scope.currentItens.filter(function (item){
          return (item != product);
        })
        $scope.currentItens = $rootScope.cart;
      }
    }

  }])
  .controller("ProductsCtrl", ['$location', '$scope', '$rootScope', '$route', 'Products', function ($location, $scope, $rootScope, $route, Products){
    $scope.buyItem = function (item){
      if(!$scope.quantity || $scope.quantity < 1){
        $scope.quantity = 1;
      }
      if(!$rootScope.cart){
        $rootScope.cart = [];
      }

      $rootScope.$broadcast("PRODUCT_ADD", {Product: $scope.data, ProductId: $scope.data.Id, Quantity: $scope.quantity});

    }
    Products.show({id: $route.current.params.id}).$promise.then(function (data){
      $scope.data = data;
    }).catch(function (e){
      $scope.data = "Error to load product details";
    })
  }])
  .controller("HomeCtrl", ['$location', '$scope', 'Products', function($location, $scope, Products){
    $scope.currentPage = 1;
    $scope.per_page = 10;
    $scope.totalPages =
    $scope.content = [];
    $scope.loadPage = function (){
      Products.index({page: $scope.currentPage, per_page: $scope.per_page}).$promise.then(function (data){
        $scope.content = data.content;
        $scope.totalPages = data.totalPages;
      }).catch(function (e){
        console.log(e);
      })
    }
    $scope.nextPage = function (){
      if($scope.currentPage+1 <= $scope.totalPages){
        $scope.currentPage += 1;
        $scope.loadPage();
      }
    }
    $scope.prevPage = function (){
      if($scope.currentPage-1 >= 1){
        $scope.currentPage -= 1;
        $scope.loadPage();
      }
    }

    $scope.loadPage();
  }])
