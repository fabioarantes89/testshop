var routes = [
  {
    route: '/products/:id',
    templateUrl: '/views/products-show.html',
    controller: 'ProductsCtrl',
    action: 'LIST'
  },
  {
    route: '/cart',
    templateUrl: '/views/cart-form.html',
    controller: 'CartCtrl',
    action: 'INDEX'
  },
  {
    route: '/login',
    templateUrl: '/views/login.html',
    controller: 'LoginCtrl',
    action: 'INDEX'
  },
  {
    route: '/addresses',
    templateUrl: '/views/addresses.html',
    controller: 'AddressesCtrl',
    action: 'INDEX'
  },
  {
    route: '/orders/:id',
    templateUrl: '/views/orders.html',
    controller: 'OrdersCtrl',
    action: 'INDEX'
  },
  {
    route: '/orders',
    templateUrl: '/views/orders.html',
    controller: 'OrdersCtrl',
    action: 'INDEX'
  },
  {
    route: '/checkout',
    templateUrl: '/views/checkout.html',
    controller: 'CheckoutCtrl',
    action: 'INDEX'
  },
  {
    route: '/signUp',
    templateUrl: '/views/signup.html',
    controller: 'SignupCtrl',
    action: 'INDEX'
  },
  {
    route: '/logout',
    templateUrl: '/views/logout.html',
    controller: 'LogoutCtrl',
    action: 'INDEX'
  },
  {
    route: '/',
    templateUrl: '/views/home.html',
    controller: 'HomeCtrl',
    action: 'INDEX'
  }
]
