<?php
namespace API\Controllers;
require_once("lib/RestController.php");
use TestShop\Controllers as Controllers;
use \TestShop\Routes as Routes;
use \TestShop\Models as Models;

class CouponsController extends Controllers\RestController {

  public function index(Routes\RouteAction $route){
    // Auth
    //$this->getAuthorization();
    $coupon = null;
    $json = file_get_contents('php://input');
    $obj = json_decode($json);

    $couponData = $obj->coupon ?? $_POST['coupon'];

    if(!empty($couponData)){
      $couponQuery = Models\CouponQuery::create()->filterByStatus(1)->findByCode($couponData);
      if($couponQuery[0]->alreadyUsed()){
        $ret = new \StdClass();
        $ret->status = "ERROR";
        $ret->message = "COUPON ALREADY USED";
        return $this->renderJSON($ret, 200);
      } else {
        $coupon = $couponQuery[0];
        return $this->renderJSON($coupon->toJSON(), 200);
      }
    } else {
      $this->renderError("Coupon not found", 500);
    }

  }


}
?>
