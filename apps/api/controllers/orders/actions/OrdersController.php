<?php
namespace API\Controllers;
require_once("lib/RestController.php");
use TestShop\Controllers as Controllers;
use \TestShop\Routes as Routes;
use \TestShop\Models as Models;

class OrdersController extends Controllers\RestController {

  public function index(Routes\RouteAction $route){
    // Auth
    $this->getAuthorization();

    $this->paginationVars();

    $query = Models\OrderQuery::create()->filterByUserId($this->user->getId())->joinWith("address");

    $this->content = $query->paginate($this->page, $this->perPage);

    $ret = new \StdClass();
    $ret->content = array();
    foreach($this->content as $content){
      $ret->content[] = $content->toJSON();
    }

    $ret->totalPages = $this->content->getLastPage();
    $ret->currentPage = $this->content->getPage();


    $this->renderJSON($ret, 200);
  }
  public function show(Routes\RouteAction $route){
    $this->getAuthorization();
    $id = $route->getVars()['ID'];

    $query = Models\OrderQuery::create()->filterById($id)->filterByUserId($this->user->getId())->joinWith("address")->joinWith("OrderList")->find();

    if(!empty($query[0])){
      $this->item = $query[0];
      $this->item->getBillingAddress();
      $this->item->getCoupon();


      return $this->renderJSON($this->item->toJSON(), 200);
    }
    $ret = new \StdClass();
    $ret->status = "ERROR";
    $ret->message = "Content not Found";
    $this->renderJSON($ret, 404);

  }
  public function create(Routes\RouteAction $route){
    $this->getAuthorization();
    //$date =
    // Address ID, Billing ID, Coupon if Have
    // List
    //    -  Product ID, Quantity

    $json = file_get_contents('php://input');
    $obj = json_decode($json);
    $values = $obj;


    $findAddresses = array();
    $findAddresses[] = $values->billingId;
    if(!empty($values->addressId) && $values->addressId != $values->billingId){
      $findAddresses[] = $values->addressId;
    }


    // User Addresses
    $addresses = Models\UserAddressQuery::create()->filterByStatus(1)->filterByPrimaryKeys($findAddresses)->findByUserId($this->user->getId());
    $billing = null;
    $address = null;
    foreach($addresses as $item){
      if($item->getId() == $values->billingId){
        $billing = $item;
        if(count($findAddresses) < 2){
          $address = $item;
        }
      } else if($item->getId() == $values->addressId){
        $address = $item;
      }
    }
    // Has Coupon?
    $coupon = null;
    if(!empty($values->coupon)){
      $couponQuery = Models\CouponQuery::create()->filterByStatus(1)->findByCode($values->coupon);
      if(!$couponQuery[0]){
        $this->renderError("Coupon not found", 500);
      }
      if($couponQuery[0]->alreadyUsed()){
        $ret = new \StdClass();
        $ret->status = "ERROR";
        $ret->message = "COUPON ALREADY USED";
        return $this->renderJSON($ret, 500);
      } else {
        $coupon = $couponQuery[0];
      }
    }
    if(!is_array($values->products)){
          return $this->renderError("Products must be a Array", 500);
    }
    if(!is_array($values->quantities)){
          return $this->renderError("Quantities must be a Array", 500);
    }
    if(count($values->quantities) != count($values->products)){
          return $this->renderError("Products and Quantities must have the same amount of data", 500);
    }

    $products = Models\ProductQuery::create()->filterByPrimaryKeys($values->products)->filterByStatus(1)->find();


    if($products->count() < count($values->products)){
      return $this->renderError("Some products are not available right now", 500);
    }


    $order = new Models\Order();
    $order->setDate(date("Y-m-d H:i:s"));
    $order->setUser($this->user);
    $order->setBillingAddressId($billing->getId());
    $order->setAddressId($address->getId());

    if(!empty($coupon)){
      $order->setCouponId($coupon->getId());
    }

    foreach($values->products as $key => $value){
      $orderList = new Models\OrderList();
      $orderList->setProductId($value);
      $orderList->setQuantity($values->quantities[$key]);
      $order->addOrderList($orderList);
    }




    if($order->validate()){
      $isSimulation = $route->getVars()['ONLY_SIMULATE'] ?? false;
      if(!empty($isSimulation) && $isSimulation == true){
          return $this->renderJSON($order->toJSON(), 200);
      }
      if($order->save()){
        return $this->renderJSON($order->toJSON(), 201);
      } else {
        return $this->renderError("Error creating order", 500);
      }

    } else {
      $this->errors = array();
      foreach ($this->content->getValidationFailures() as $failure) {
        $this->errors[] = sprintf("%s: %s", $failure->getPropertyPath(), $failure->getMessage());
      }
      return $this->renderError($this->errors, 403);
    }

  }

}
?>
