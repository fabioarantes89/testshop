<?php
namespace API\Controllers;
require_once("lib/RestController.php");
use TestShop\Controllers as Controllers;
use \TestShop\Routes as Routes;
use \TestShop\Models as Models;

class UsersController extends Controllers\RestController {

  public function me(Routes\RouteAction $route){
    $this->getAuthorization();
    return $this->renderJSON($this->user->toJSON(), 200);
  }

  public function addresses(Routes\RouteAction $route){
    $this->getAuthorization();

    $addresses = Models\UserAddressQuery::create()->filterByStatus(1)->findByUserId($this->user->getId());

    $ret = new \StdClass();
    $ret->content = array();
    foreach($addresses as $address){
      $ret->content[] = $address->toJSON();
    }

    if(count($ret->content) > 0){
      return $this->renderJSON($ret, 200);
    }
  }
  public function create_address(Routes\RouteAction $route){
    $this->getAuthorization();

    $json = file_get_contents('php://input');
    $obj = json_decode($json);

    $values = $obj;

    $this->content = new Models\UserAddress();
    $this->content->setFullname($values->fullName);
    $this->content->setAddressline($values->addressLine);
    $this->content->setCity($values->city);
    $this->content->setProvince($values->province);
    $this->content->setCountry($values->country);
    $this->content->setZipCode($values->zipCode);
    $this->content->setTelephone($values->telephone);
    $this->content->setUser($this->user);
    if($this->content->validate()){
      $this->content->save();
      return $this->renderJSON($this->content->toJSON(), 201);
    } else {
      $ret = new \StdClass();
      $ret->status = "ERROR";
      $ret->data = array();
      foreach ($this->content->getValidationFailures() as $failure) {
        $ret->data[] = sprintf("%s: %s", ucfirst(str_replace("_", " ", $failure->getPropertyPath())), $failure->getMessage());
      }
      $this->renderJSON($ret, 500);
    }

  }
  public function create(Routes\RouteAction $route){
    $json = file_get_contents('php://input');
    $obj = json_decode($json);

    $values = $obj;

    foreach(array("name" => "Name", "email" => "Email", "pass" => "Password") as $key => $value){
      if(empty($values->$key)){
        $this->renderError("$value can't be blank", 500);
      }
    }

    $this->content = new Models\User();
    $this->content->setName($values->name);
    $this->content->setEmail($values->email);
    $this->content->setSalt($values->pass);
    $this->content->setType(1);
    $this->content->setStatus(1);

    if($this->content->validate()){
      $this->content->save();
      return $this->renderJSON($this->content->toJSON(), 201);
    } else {
      $ret = new \StdClass();
      $ret->status = "ERROR";
      $ret->data = array();
      foreach ($this->content->getValidationFailures() as $failure) {
        $ret->data[] = sprintf("%s: %s", ucfirst(str_replace("_", " ", $failure->getPropertyPath())), $failure->getMessage());
      }
      $this->renderJSON($ret, 500);
    }

  }

}
?>
