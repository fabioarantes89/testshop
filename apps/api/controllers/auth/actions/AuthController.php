<?php
namespace API\Controllers;
require_once("lib/DefaultController.php");
use TestShop\Controllers as Controllers;
use \TestShop\Routes as Routes;
use \TestShop\Models as Models;

class AuthController extends Controllers\DefaultController {
  public function token(Routes\RouteAction $route){
    $ret = new \StdClass();
    $ret->status = "ERROR";

    $json = file_get_contents('php://input');
    $values = json_decode($json);


    foreach(array("appKey", "appSecret", "email", "pass") as $field){
      if(empty($values->$field)){
        $ret->message = "{$field} can't be blank";
        return $this->renderJSON($ret, 500);
      }
    }

    $apps = Models\AppQuery::create()->findByAppKey($values->appKey);

    if($apps->count() < 1){
      $ret->message = "App not Found";
      return $this->renderJSON($ret, 403);
    }


    foreach($apps as $app){
      if($app->getSecret() != $values->appSecret){
        $ret->message = "App not Found";
        return $this->renderJSON($ret, 403);
      } else {
          $this->App = $app;
      }
    }

    // At this point is only need to authenticate user.

    $user = Models\UserQuery::create()->findByEmail($values->email);
    if($user->count() < 1 || $user[0]->getSalt() != md5($values->pass)){
      $ret->message = "User or password invalid!";
      return $this->renderJSON($ret, 403);
    } else {
      $this->user = $user[0];
    }

    $token = new Models\Token();
    $token->setUser($this->user);
    $token->setApp($this->App);
    if($token->save()){

      return $this->renderJSON($token->toObject(), 201);
    } else {
      $ret->message = "Unable to create Token";
      return $this->renderJSON($ret, 501);
    }

  }
}
?>
