<?php
namespace API\Controllers;
require_once("lib/RestController.php");
use TestShop\Controllers as Controllers;
use \TestShop\Routes as Routes;
use \TestShop\Models as Models;

class ProductsController extends Controllers\RestController {

  public function index(Routes\RouteAction $route){
    // Auth
    //$this->getAuthorization();

    $this->paginationVars();

    $query = Models\ProductQuery::create()->filterByStatus(1);
    //$this->defaultFilters($query);
    $this->content = $query->paginate($this->page, $this->perPage);

    $ret = new \StdClass();
    $ret->content = array();
    foreach($this->content as $content){
      $ret->content[] = $content->toJSON();
    }


    $ret->totalPages = $this->content->getLastPage();
    $ret->currentPage = $this->content->getPage();


    $this->renderJSON($ret, 200);
  }
  public function show(Routes\RouteAction $route){
    $id = $route->getVars()['ID'];

    $query = Models\ProductQuery::create()->findPk($id);
    //$this->defaultFilters($query);
    if($query){
      $this->content = $query->toJSON(false);
      return $this->renderJSON($this->content, 200);
    }
    $ret = new \StdClass();
    $ret->status = "ERROR";
    $ret->message = "Content not Found";
    $this->renderJSON($ret, 404);

  }


}
?>
