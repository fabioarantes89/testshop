<?php
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/oauth\/token/i", "auth", "token", null, "POST"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/users/i", "users", "create", null, "POST"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/me\/addresses/i", "users", "create_address", null, "POST"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/me\/addresses/i", "users", "addresses", null, "GET"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/me\/orders\/([0-9]+)(\/show)?/i", "orders", "show", array("ID" => ":1"), "GET"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/me\/orders\/summary/i", "orders", "create", array("ONLY_SIMULATE" => true), "POST"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/me\/orders/i", "orders", "create", null, "POST"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/me\/orders/i", "orders", "index", null, "GET"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/me/i", "users", "me", null, "GET"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/coupons/i", "coupons", "index", null, "POST"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/products\/([0-9]+)(\/show)?/i", "products", "show", array("ID" => ":1"), "GET"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/products/i", "products", "index", null, "GET"));



?>
