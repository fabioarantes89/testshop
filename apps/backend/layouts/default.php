<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>TestShop</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/style.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">TestShop</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo($this->generateURL("/")); ?>">Home</a></li>
            <li><a href="<?php echo($this->generateURL("/users/")); ?>">Users</a></li>
            <li><a href="<?php echo($this->generateURL("/products/")); ?>">Products</a></li>
            <li><a href="<?php echo($this->generateURL("/orders/")); ?>">Orders</a></li>
            <li><a href="<?php echo($this->generateURL("/addresses/")); ?>">Addresses</a></li>
            <li><a href="<?php echo($this->generateURL("/coupons/")); ?>">Coupons</a></li>
            <li><a href="<?php echo($this->generateURL("/apps/")); ?>">Apps</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo($this->userLogged['Name']); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo($this->generateURL("/logout")); ?>">Logout</a></li>
                </ul>
              </li>
            </ul>
        </div><!--/.nav-collapse -->



      </div>
    </nav>

    <div class="container">

      <div id="content">
        <?php echo($this->content); ?>
      </div>

    </div><!-- /.container -->


</body>
</html>
