<?php if(!empty($this->message)){ ?>
  <div class="alert <?php echo($this->message->getAlertClass()); ?>" role="alert"><?php echo($this->message->text); ?></div>
<?php } ?>
<?php if(!empty($this->errors)){ ?>
  <div class="alert alert-danger" role="alert">
    <ul>
  <?php foreach($this->errors as $error){ ?>
    <li><?php echo($error->text); ?></li>
  <?php } ?>
  </ul>
</div>

<?php } ?>
