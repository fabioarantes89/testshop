<?php if($this->content->haveToPaginate()){ ?>
<div class="row">
  <div class="col-lg-12">
    <nav id="pagination">
      <ul class="pagination">
        <?php $links = $this->content->getLinks(5); ?>
        <?php if(!$this->content->isFirstPage()){ ?>
          <li><a class="btn btn-sm btn-default" href="<?php echo($this->paginationLink.($this->content->getFirstPage())); ?>">&laquo;</a></li>
        <?php } ?>

        <?php foreach($links as $i){ ?>
          <li <?php if($this->content->getPage() == $i){ ?>class="active"<?php } ?>><a class="btn btn-sm btn-default" href="<?php echo($this->paginationLink.($i)); ?>"><?php echo($i); ?></a></li>
        <?php } ?>

        <?php if(!$this->content->isLastPage()){ ?>
          <li><a class="btn btn-sm btn-default" href="<?php echo($this->paginationLink.($this->content->getLastPage())); ?>">&raquo;</a></li>
        <?php } ?>
      </ul>
    </nav>
  </div>
</div>
<?php } ?>
