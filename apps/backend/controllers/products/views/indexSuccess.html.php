<div class="row">
  <div class="col-lg-12">
    <h1>Listing products</h1>
  </div>
  <div class="col-lg-12">
    <div>
      <?php $this->includePartial("home", "messages"); ?>
      <p>
        <a href="<?php echo($this->generateURL("/products/new")); ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span>New Product</a>
      </p>
    </div>
    <table class="table table-striped">
      <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th class="act">Actions</th>
          </tr>
      </thead>
      <tbody>
        <?php foreach($this->content as $content){ ?>
        <tr>
          <td><a href="<?php echo($this->generateURL("/products/".($content->getID())."/edit")); ?>"><?php echo($content->getID()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/products/".($content->getID())."/edit")); ?>"><?php echo($content->getTitle()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/products/".($content->getID())."/edit")); ?>"><?php echo($content->formatCurrency()); ?></a></td>
          <td>
            <a href="<?php echo($this->generateURL("/products/".($content->getID())."/edit")); ?>" class="btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>

    <?php $this->includePartial("home", "pagination"); ?>
  </div>
</div>
