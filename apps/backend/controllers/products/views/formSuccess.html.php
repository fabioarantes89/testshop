<div class="row">
  <form method="post" enctype="multipart/form-data">
    <?php $this->includePartial("home", "messages"); ?>
    <div class="col-lg-12">
      <h2>Edit Product: <?php echo($this->content->getTitle()); ?></h2>
    </div>
    <div class="col-lg-8">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Product Info</h5>
        </div>
        <div class="panel-body">

            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" class="form-control" id="name" name="title" placeholder="Title" value="<?php echo(htmlspecialchars($this->content->getTitle())); ?>"/>
            </div>

            <div class="form-group">
              <label for="text">Text</label>
              <textarea class="form-control" id="text" name="text" placeholder="Text" ><?php echo($this->content->getText()); ?></textarea>
            </div>

            <div class="form-group">
              <label for="status">Status</label>
              <input type="text" class="form-control" id="status" name="status" placeholder="Status" value="<?php echo($this->content->getStatus()); ?>" />
            </div>
            <div class="form-group">
              <label for="date">Date</label>
              <input type="datetime" class="form-control" id="status" name="date" placeholder="Date" value="<?php echo($this->content->getDate()->format("d-m-Y H:i:s")); ?>" />
            </div>
            <div class="form-group">
              <label for="currency">Currency</label>
              <input type="text" class="form-control" id="currency" name="currency" placeholder="Currency" value="<?php echo($this->content->getCurrency()); ?>" />
            </div>
            <div class="form-group">
              <label for="price">Price</label>
              <input type="text" class="form-control" id="price" name="price" placeholder="Price" value="<?php echo($this->content->getPrice()); ?>" />
            </div>


            <button class="btn btn-primary" type="submit">Save</button>
            <a href="<?php echo($this->generateURL("/products/")); ?>" class="btn btn-danger">Cancel</a>


        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Image Thumb</h5>
        </div>
        <div class="panel-body">
          <?php
            $path = $this->content->getFile();
            if(!empty($path)){
          ?>
          <img src="<?php echo($path); ?>" class="img-rounded" />
          <?php } ?>
          <div class="form-group">
            <label for="file">Change Image</label>
            <input type="hidden" name="file" value="<?php echo($this->content->getFile()); ?>" />
            <input type="file" class="form-control" id="file" name="new_file" placeholder="File" />
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-lg-1">
                <input type="checkbox" id="remove_file" name="removeImg" placeholder="Remove Image" />
              </div>
              <div class="col-lg-10">
                <label for="remove_file">Remove File</label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
