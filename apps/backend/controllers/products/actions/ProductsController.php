<?php
  namespace BackEnd\Controllers;
  require_once("lib/DefaultController.php");


  use TestShop\Controllers as Controllers;
  use \TestShop\Routes as Routes;
  use \TestShop\Models as Models;

  class ProductsController extends Controllers\DefaultController {
    public function index(Routes\RouteAction $route){
      $this->paginationVars();
      $query = Models\ProductQuery::create();
      $this->defaultFilters($query);
      $this->content = $query->paginate($this->page, $this->perPage);

      $this->render("index");
    }
    public function edit(Routes\RouteAction $route){

      $id = $route->getVars()['ID'];

      $this->content = Models\ProductQuery::create()->findPk($id);
      if(!empty($_POST)){
          $this->proccessForm($_POST);
      }

      $this->render("form");
    }
    public function create(Routes\RouteAction $route){

      $id = $route->getVars()['ID'];

      $this->content = new Models\Product;
      $this->content->setDate(date("Y-m-d H:i:s"));
      if(!empty($_POST)){
          $this->proccessForm($_POST);
      }
      $this->render("form");
    }
    public function delete(Routes\RouteAction $route)
    {
        $id = $route->getVars()['ID'];

        $this->content = Models\ProductQuery::create()->findPk($id);
        if($this->content){
          $this->content->delete();
          if(!$this->content->isDeleted()){
              throw new Routes\RouteException("Error updating data", $this->content);
          } else {
            $this->redirect("/products/index.php");
          }
        } else {
          throw new Routes\RouteException("Content not found");
        }
    }
    protected function proccessForm($values){
      //var_dump($_FILES);
      $oldFile = $this->content->getFile();
      if($url = $this->saveUpload("new_file")){
        $this->content->setFile($url);
      }
      if(!empty($values['removeImg']) || !empty($url)){
        if(\TestShop\Application::$app->deleteFile($oldFile) && empty($url)){
            $this->content->setFile("");
        }
      }

      $this->content->setTitle($values['title']);
      $this->content->setText($values['text']);
      $this->content->setStatus($values['status']);
      $this->content->setDate($values['date']);
      $this->content->setCurrency($values['currency']);
      $this->content->setPrice($values['price']);

      if($this->content->validate()){
        $this->content->save();
        \TestShop\Dispatcher::setSessionValue("message_success", "Saved Successfully!");
        $this->redirect("/products/index.php");
      } else {
        $this->errors = array();
        foreach ($this->content->getValidationFailures() as $failure) {
          $this->errors[] = new \TestShop\UIAlerts("error", sprintf("%s: %s", $failure->getPropertyPath(), $failure->getMessage()));
        }
      }
    }
}
?>
