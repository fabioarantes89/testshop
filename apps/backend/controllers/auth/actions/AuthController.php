<?php
namespace BackEnd\Controllers;
require_once("lib/DefaultController.php");
use TestShop\Controllers as Controllers;
use \TestShop\Routes as Routes;
use \TestShop\Models as Models;

class AuthController extends Controllers\DefaultController {
  public $layout = "login";
  public function login(Routes\RouteAction $route){
    if(!empty($_POST['email'] && $_POST['password'])){
      $query = Models\UserQuery::create()->filterByEmail($_POST['email'])->filterByType(1)->limit(1)->find();
      $salt = md5($_POST['password']);
      if($query->count() > 0){
          foreach($query as $user){
            if($user->getSalt() == $salt){
              \TestShop\Dispatcher::setUser($user->toArray());
              \TestShop\Dispatcher::redirect("/");
              return;
            }
          }
      }
      $this->errors = array(new \TestShop\UIAlerts("error", "User or password is invalid"));
    }
    $this->render("login");
  }
  public function logout(Routes\RouteAction $route){
    \TestShop\Dispatcher::unsetUser();
    $this->redirect("login");
  }
}
?>
