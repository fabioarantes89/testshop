<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-4 col-lg-offset-4">
      <?php $this->includePartial("home", "messages"); ?>
      
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Login</h5>
        </div>
        <div class="panel-body">
          <form method="post">
            <div class="form-group">
              <label for="email">E-mail</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="<?php echo(htmlspecialchars((!empty($_POST['email']))?$_POST['email']:"")); ?>"/>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
            </div>
            <button class="btn btn-primary">Login</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
