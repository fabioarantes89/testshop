<?php
  namespace BackEnd\Controllers;
  require_once("lib/DefaultController.php");


  use TestShop\Controllers as Controllers;
  use \TestShop\Routes as Routes;
  use \TestShop\Models as Models;

  class AppsController extends Controllers\DefaultController {
    public function index(Routes\RouteAction $route){
      $this->paginationVars();

      $query = Models\AppQuery::create();
      $this->defaultFilters($query);
      if(!empty($q)){
        $query = $query->filterByName($q."%");
      }

      $this->content = $query->paginate($this->page, $this->perPage);
      $this->render("index");
    }
    public function edit(Routes\RouteAction $route)
    {
        $id = $route->getVars()['ID'];

        $this->content = Models\AppQuery::create()->findPk($id);
        if(!empty($_POST['content'])){
            $this->proccessForm($_POST['content']);
        }


        $this->render("form");
    }
    public function create(Routes\RouteAction $route)
    {
        $this->content = new Models\App();

        if(!empty($_POST)){
            if($this->proccessForm($_POST)){
              \TestShop\Dispatcher::setSessionValue("message_success", "Saved Successfully!");
              $this->redirect("/apps/".$this->content->getId()."/edit");
            }
        }


        $this->render("form");
    }
    public function delete(Routes\RouteAction $route)
    {
        $id = $route->getVars()['ID'];

        $this->content = Models\AppQuery::create()->findPk($id);
        if($this->content){
          $this->content->delete();
          if(!$this->content->isDeleted()){
              throw new Routes\RouteException("Error updating data", $this->content);
          } else {
            $this->redirect("/apps/index.php");
          }
        } else {
          throw new Routes\RouteException("Content not found");
        }
    }
    public function proccessForm($data){
      $this->content->setName($data['name']);
      if($this->content->validate()){
        $this->content->save();
        return true;
      } else {
        $this->errors = array();
        foreach ($this->content->getValidationFailures() as $failure) {
          $this->errors[] = new \TestShop\UIAlerts("error", sprintf("%s: %s", $failure->getPropertyPath(), $failure->getMessage()));
        }

      }
      return false;
    }

  }
?>
