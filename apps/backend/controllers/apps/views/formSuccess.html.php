<div class="row">
  <form method="post" enctype="multipart/form-data">
    <?php $this->includePartial("home", "messages"); ?>
    <div class="col-lg-12">
      <?php if($this->content->isNew()){ ?>
        <h2>New app</h2>
      <?php } else { ?>
        <h2>Edit app: <?php echo($this->content->getName()); ?></h2>
      <?php } ?>

    </div>
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>App info</h5>
        </div>
        <div class="panel-body">

            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo(htmlspecialchars($this->content->getName())); ?>"/>
            </div>
            <?php if(!$this->content->isNew()){ ?>
            <div class="form-group">
              <label for="secret">Secret</label>
              <input type="text" disabled="disabled" class="form-control" id="secret" name="secret" placeholder="Type" value="<?php echo(htmlspecialchars($this->content->getSecret())); ?>"/>
            </div>
            <div class="form-group">
              <label for="secret">App Key</label>
              <input type="text" disabled="disabled" class="form-control" id="appKey" name="app_key" placeholder="Type" value="<?php echo(htmlspecialchars($this->content->getAppKey())); ?>"/>
            </div>
            <?php } ?>

            <button class="btn btn-primary" type="submit">Save</button>
            <a href="/apps/index.php" class="btn btn-danger">Cancel</a>
        </div>
      </div>
    </div>
  </form>
</div>
