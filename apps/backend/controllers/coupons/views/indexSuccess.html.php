<div class="row">
  <div class="col-lg-12">
    <h1>Listing coupons</h1>
  </div>
  <div class="col-lg-12">
    <div>
      <?php $this->includePartial("home", "messages"); ?>
      <p>
        <a href="<?php echo($this->generateURL("/coupons/new")); ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span>New Coupon</a>
      </p>
    </div>
    <table class="table table-striped">
      <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Already Used?</th>
            <th class="act">Actions</th>
          </tr>
      </thead>
      <tbody>
        <?php foreach($this->content as $content){ ?>
        <tr>
          <td><a href="<?php echo($this->generateURL("/coupons/".$content->getID())."/edit"); ?>"><?php echo($content->getID()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/coupons/".$content->getID())."/edit"); ?>"><?php echo($content->getCode()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/coupons/".$content->getID())."/edit"); ?>"><?php echo($this->formatYesNo($content->alreadyUsed())); ?></a></td>
          <td>
            <a href="<?php echo($this->generateURL("/coupons/".($content->getID())."/edit")); ?>" class="btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
            <?php if(!$content->alreadyUsed()){?>
              <a href="<?php echo($this->generateURL("/coupons/".($content->getID())."/delete")); ?>" class="btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</a>
            <?php } ?>
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>

    <?php $this->includePartial("home", "pagination"); ?>
  </div>
</div>
