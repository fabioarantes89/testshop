<div class="row">
  <form method="post" enctype="multipart/form-data">
    <?php $this->includePartial("home", "messages"); ?>
    <div class="col-lg-12">
      <?php if($this->content->isNew()){ ?>
        <h2>New coupon</h2>
      <?php } else { ?>
        <h2>Edit coupon: <?php echo($this->content->getCode()); ?></h2>
      <?php } ?>

    </div>
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Coupon info</h5>
        </div>
        <div class="panel-body">
            <?php if(!$this->content->isNew()){ ?>
            <div class="form-group">
              <label for="code">Code</label>
              <input type="text" disabled="disabled" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo(htmlspecialchars($this->content->getCode())); ?>"/>
            </div>
            <?php } ?>
            <div class="form-group">
              <label for="title">Type</label>
              <input type="text" class="form-control" id="type" name="type" placeholder="Type" value="<?php echo(htmlspecialchars($this->content->getType())); ?>"/>
            </div>
            <div class="form-group">
              <label for="currency">Currency</label>
              <input type="text" class="form-control" id="currency" name="currency" placeholder="Currency" value="<?php echo(htmlspecialchars($this->content->getCurrency())); ?>"/>
            </div>
            <div class="form-group">
              <label for="discount">Value</label>
              <input type="text" class="form-control" id="discount" name="discount" placeholder="Currency" value="<?php echo(htmlspecialchars($this->content->getDiscount())); ?>"/>
            </div>

            <button class="btn btn-primary" type="submit">Save</button>
            <a href="<?php echo($this->generateURL("/coupons/")); ?>" class="btn btn-danger">Cancel</a>


        </div>
      </div>
    </div>
  </form>
</div>
