<?php
  namespace BackEnd\Controllers;
  require_once("lib/DefaultController.php");


  use TestShop\Controllers as Controllers;
  use \TestShop\Routes as Routes;
  use \TestShop\Models as Models;

  class CouponsController extends Controllers\DefaultController {
    public function index(Routes\RouteAction $route){
      $this->paginationVars();
      $query = Models\CouponQuery::create();
      $this->defaultFilters($query);
      $this->content = $query->paginate($this->page, $this->perPage);

      $this->render("index");
    }
    public function create(Routes\RouteAction $route){

      $this->content = new Models\Coupon();

      if(!empty($_POST)){
          $this->proccessForm($_POST);
      }
      $this->render("form");
    }
    public function delete(Routes\RouteAction $route){

      $id = $route->getVars()['ID'];

      $this->content = Models\CouponQuery::create()->findPk($id);

      if($this->content){
        if(!$this->content->alreadyUsed()){
          $this->content->delete();
          if(!$this->content->isDeleted()){
              throw new Routes\RouteException("Error updating data", $this->content);
          } else {
            \TestShop\Dispatcher::setSessionValue("message_success", "Saved Successfully!");
            $this->redirect("/coupons/index.php");
          }
        } else {
          \TestShop\Dispatcher::setSessionValue("message_error", "You can't delete an used coupon!");
          $this->redirect("/coupons/index.php");

        }
      } else {
        throw new Routes\RouteException("Content not found");
      }

      $this->index($route);
      //$this->render("index");
    }
    public function edit(Routes\RouteAction $route){

      $id = $route->getVars()['ID'];

      $this->content = Models\CouponQuery::create()->findPk($id);
      if(!empty($_POST)){
          $this->proccessForm($_POST);
      }

      $this->render("form");
    }
    public function proccessForm($values){

      $this->content->setType($values['type']);
      $this->content->setCurrency($values['currency']);
      $this->content->setDiscount($data['discount']);

      if($this->content->validate()){
        $this->content->save();
        \TestShop\Dispatcher::setSessionValue("message_success", "Saved Successfully!");
        $this->redirect("/coupons/index.php");
      } else {
        $this->errors = array();
        foreach ($this->content->getValidationFailures() as $failure) {
          $this->errors[] = new \TestShop\UIAlerts("error", sprintf("%s: %s", $failure->getPropertyPath(), $failure->getMessage()));
        }
      }

    }

}
?>
