<?php
  namespace BackEnd\Controllers;
  require_once("lib/DefaultController.php");


  use TestShop\Controllers as Controllers;
  use \TestShop\Routes as Routes;
  use \TestShop\Models as Models;

  class AddressesController extends Controllers\DefaultController {
    public function index(Routes\RouteAction $route){
      $this->paginationVars();
      $query = Models\UserAddressQuery::create();
      $this->defaultFilters($query);
      if(!empty($route->getVars()['USER_ID'])){
        $query = $query->useUserQuery()->filterByPrimaryKey($route->getVars()['USER_ID'])->endUse();
        $this->user = Models\UserQuery::create()->findPk($route->getVars()['USER_ID']);
      }

      $this->content = $query->paginate($this->page, $this->perPage);

      $this->render("index");
    }
    public function edit(Routes\RouteAction $route){
      $id = $route->getVars()['ID'];

      $this->content = Models\UserAddressQuery::create()->findPk($id);

      if(!empty($_POST['content'])){
          $this->proccessForm($_POST['content']);
      }

      $this->render("form");
    }
    public function delete(Routes\RouteAction $route)
    {
        $id = $route->getVars()['ID'];

        $this->content = Models\UserAddressQuery::create()->findPk($id);
        if($this->content){
          $this->content->delete();
          if(!$this->content->isDeleted()){
              throw new Routes\RouteException("Error updating data", $this->content);
          } else {
            $this->redirect("/addresses/index.php");
          }
        } else {
          throw new Routes\RouteException("Content not found");
        }
    }
    private function proccessForm($values){

      $this->content->setFullname($values['fullName']);
      $this->content->setAddressline($values['addressLine']);
      $this->content->setCity($values['city']);
      $this->content->setProvince($values['province']);
      $this->content->setCountry($values['country']);
      $this->content->setZipCode($values['zipCode']);
      $this->content->setTelephone($values['telephone']);
      $this->content->setUserId($values['user_id']);

      if($this->content->validate()){
        $this->content->save();
        \TestShop\Dispatcher::setSessionValue("message_success", "Saved Successfully!");
        $this->redirect("/users/{$this->content->getUserId()}/addresses");
      } else {
        $this->errors = array();
        foreach ($this->content->getValidationFailures() as $failure) {
          $this->errors[] = new \TestShop\UIAlerts("error", sprintf("%s: %s", ucfirst(str_replace("_", " ", $failure->getPropertyPath())), $failure->getMessage()));
        }
      }

    }
}
?>
