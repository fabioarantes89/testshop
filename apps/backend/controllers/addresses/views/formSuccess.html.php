<div class="row">

  <?php $this->includePartial("home", "messages"); ?>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h3>Edit Address: <?php echo($this->content->getAddressLine()); ?></h3>
    </div>
    <div class="panel-body">
      <form method="post">
        <div class="form-group">
          <label for="fullName">Full Name</label>
          <input type="text" class="form-control" id="fullName" name="content[fullName]" placeholder="Full Name" value="<?php echo($this->content->getFullName()); ?>"/>
        </div>
        <div class="form-group">
          <label for="addressLine">Address Line</label>
          <input type="text" class="form-control" id="addressLine" name="content[addressLine]" placeholder="Address Line" value="<?php echo($this->content->getAddressLine()); ?>"/>
        </div>
        <div class="form-group">
          <label for="city">City</label>
          <input type="text" class="form-control" id="city" name="content[city]" placeholder="City" value="<?php echo($this->content->getCity()); ?>"/>
        </div>

        <div class="form-group">
          <label for="province">Province</label>
          <input type="text" class="form-control" id="province" name="content[province]" placeholder="City" value="<?php echo($this->content->getProvince()); ?>"/>
        </div>
        <div class="form-group">
          <label for="country">Country</label>
          <input type="text" class="form-control" id="country" name="content[country]" placeholder="Country" value="<?php echo($this->content->getCountry()); ?>"/>
        </div>
        <div class="form-group">
          <label for="zipCode">Zip Code</label>
          <input type="text" class="form-control" id="zipCode" name="content[zipCode]" placeholder="Zip Code" value="<?php echo($this->content->getZipCode()); ?>"/>
        </div>
        <div class="form-group">
          <label for="telephone">Telephone</label>
          <input type="text" class="form-control" id="telephone" name="content[telephone]" placeholder="Telephone" value="<?php echo($this->content->getTelephone()); ?>"/>
        </div>
        <div class="form-group">
          <label for="user_id">User</label>
          <input type="text" class="form-control" id="telephone" name="content[user_id]" placeholder="User ID" value="<?php echo($this->content->getUserId()); ?>"/>
        </div>


        <button class="btn btn-primary" type="submit">Save</button>
        <a href="<?php echo($this->generateURL("/addresses/")); ?>" class="btn btn-danger">Cancel</a>
      </form>

    </div>
  </div>
</div>
