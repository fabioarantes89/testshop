<div class="row">
  <div class="col-lg-12">
    <?php if(!$this->user){ ?>
      <h1>Listing addresses</h1>
    <?php } else { ?>
      <h1>Listing addresses from user: <?php echo($this->user->getName()); ?></h1>
    <?php } ?>

  </div>
  <div class="col-lg-12">
    <div>
      <?php $this->includePartial("home", "messages"); ?>
      <p>
        <?php if(!empty($this->user)){ ?>
          <a href="<?php echo($this->generateURL("/addresses/".$this->user->getID()."/new")); ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span>New Address</a>
        <?php } else { ?>
          <a href="<?php echo($this->generateURL("/addresses/new")); ?>" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span>New Address</a>
        <?php } ?>

      </p>
    </div>
    <table id="addresses" class="table table-striped">
      <thead>
          <tr>
            <th>ID</th>
            <th>Address Line</th>
            <th>Zip Code</th>
            <th>City</th>
            <th>Province</th>
            <th>Country</th>
            <th class="act">Actions</th>
          </tr>
      </thead>
      <tbody>
        <?php foreach($this->content as $content){
          ?>
        <tr>
          <td><a href="<?php echo($this->generateURL("/addresses/".($content->getID())."/edit")); ?>"><?php echo($content->getID()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/addresses/".($content->getID())."/edit")); ?>"><?php echo($content->getAddressLine()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/addresses/".($content->getID())."/edit")); ?>"><?php echo($content->getZipCode()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/addresses/".($content->getID())."/edit")); ?>"><?php echo($content->getCity()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/addresses/".($content->getID())."/edit")); ?>"><?php echo($content->getProvince()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/addresses/".($content->getID())."/edit")); ?>"><?php echo($content->getCountry()); ?></a></td>
          <td>
            <a href="<?php echo($this->generateURL("/addresses/".($content->getID())."/edit")); ?>" class="btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
            <?php if(!$content->isDeleted()){ ?><a href="<?php echo($this->generateURL("/addresses/".($content->getID())."/delete")); ?>" class="btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</a><?php } ?>
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>

    <?php $this->includePartial("home", "pagination"); ?>
  </div>
</div>
