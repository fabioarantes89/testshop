<?php
  namespace BackEnd\Controllers;
  require_once("lib/DefaultController.php");


  use TestShop\Controllers as Controllers;
  use Propel\Runtime\ActiveQuery\Criteria;
  use \TestShop\Routes as Routes;
  use \TestShop\Models as Models;

  class UsersController extends Controllers\DefaultController {
    public function index(Routes\RouteAction $route){
      $this->paginationVars();

      $query = Models\UserQuery::create();

      $this->defaultFilters($query);

      if(!empty($q)){
        $query = $query->filterByName($q."%");
      }

      $this->content = $query->paginate($this->page, $this->perPage);
      $this->render("index");
    }
    public function edit(Routes\RouteAction $route)
    {
        $id = $route->getVars()['ID'];

        $this->content = Models\UserQuery::create()->findPk($id);
        if(!empty($_POST['content'])){
            $this->proccessForm($_POST['content']);
        }


        $this->render("form");
    }
    public function create(Routes\RouteAction $route)
    {
        $this->content = new Models\User();

        if(!empty($_POST['content'])){
            $this->proccessForm($_POST['content']);
        }


        $this->render("form");
    }
    public function delete(Routes\RouteAction $route)
    {
        $id = $route->getVars()['ID'];

        $this->content = Models\UserQuery::create()->findPk($id);
        if($this->content){
          $this->content->delete();
          if(!$this->content->isDeleted()){
              throw new Routes\RouteException("Error updating data", $this->content);
          } else {
            $this->redirect("/users/index.php");
          }
        } else {
          throw new Routes\RouteException("Content not found");
        }
    }
    public function proccessForm($data){
      if(!empty($data['password']) && !empty($data['password_verify'])){
        $this->content->setName($data['name']);
        $this->content->setEmail($data['email']);
        $this->content->setType($data['type']);

        if($data['password'] == $data['password_verify']){
          $data['salt'] = $data['password'];
          unset($data['password_verify']);
          $this->content->setSalt($data['salt']);


          if($this->content->validate()){
            $this->content->save();
            \TestShop\Dispatcher::setSessionValue("message_success", "Saved Successfully!");
            $this->redirect("/users/index.php");
          } else {
            $this->errors = array();
            foreach ($this->content->getValidationFailures() as $failure) {
              $this->errors[] = new \TestShop\UIAlerts("error", sprintf("%s: %s", $failure->getPropertyPath(), $failure->getMessage()));
            }

          }

        } else {
          $this->errors = array();
          $this->errors[] = new \TestShop\UIAlerts("error", "Passwords mismatch!");;
        }
      }

    }

  }
?>
