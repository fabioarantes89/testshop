<div class="row">

  <?php $this->includePartial("home", "messages"); ?>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h3>Edit User: <?php echo($this->content->getName()); ?></h3>
    </div>
    <div class="panel-body">
      <form method="post">
        <input type="hidden" name="content[id]" value="<?php echo($this->content->getID()); ?>">
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="content[name]" placeholder="Name" value="<?php echo($this->content->getName()); ?>"/>
        </div>
        <div class="form-group">
          <label for="email">E-mail</label>
          <input type="email" class="form-control" id="email" name="content[email]" placeholder="E-mail" value="<?php echo($this->content->getEmail()); ?>"/>
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" name="content[password]" placeholder="Password" value="<?php echo($this->content->getSalt()) ;?>"/>
        </div>
        <div class="form-group">
          <label for="password_verify">Repeat Password</label>
          <input type="password" class="form-control" id="password_verify" name="content[password_verify]" placeholder="Password verify" value="<?php echo($this->content->getSalt()) ;?>"/>
        </div>
        <div class="form-group">
          <label for="type">Type</label>
          <input type="text" class="form-control" id="type" name="content[type]" placeholder="Type" value="<?php echo($this->content->getType()); ?>"/>
        </div>
        <button class="btn btn-primary" type="submit">Save</button>
        <a href="<?php echo($this->generateURL("/users/")); ?>" class="btn btn-danger">Cancel</a>
      </form>

    </div>
  </div>
</div>
