<?php
  namespace BackEnd\Controllers;
  require_once("lib/DefaultController.php");


  use TestShop\Controllers as Controllers;
  use \TestShop\Routes as Routes;
  use \TestShop\Models as Models;

  class OrdersController extends Controllers\DefaultController {
    public function index(Routes\RouteAction $route){
      $this->paginationVars();

      $query = Models\OrderQuery::create();

      if(!empty($route->getVars()['USER_ID'])){
        $query = $query->useUserQuery()->filterByPrimaryKey($route->getVars()['USER_ID'])->endUse();
        $this->user = Models\UserQuery::create()->findPk($route->getVars()['USER_ID']);
      }

      $this->content = $query->paginate($this->page, $this->perPage);

      $this->render("index");
    }
    public function show(Routes\RouteAction $route){
      $id = $route->getVars()['ID'];

      $this->currencyFormat = $_GET['currency'] ?? "USD";

      $this->content = Models\OrderQuery::create()->findPk($id);
      if(empty($this->content)){
        throw new ContentException("Content not found");
      }
      $this->render("show");
    }
    public function changeStatus(Routes\RouteAction $route){
      $id = $route->getVars()['ID'];
      $this->content = Models\OrderQuery::create()->findPk($id);
      $newStatus = $route->getVars()['status'];
      $this->content->setStatus($newStatus);
      if($this->content->validate()){
        $this->content->save();
      }
      $this->redirect("/orders/" . $this->content->getId() . "/show");
    }
  }
?>
