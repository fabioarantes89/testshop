<div class="row">
  <div class="col-lg-12">
    <?php if(!$this->user){ ?>
      <h1>Listing orders</h1>
    <?php } else { ?>
      <h1>Listing orders from user: <?php echo($this->user->getName()); ?></h1>
    <?php } ?>

  </div>
  <div class="col-lg-12">
    <div>
      <?php $this->includePartial("home", "messages"); ?>
    </div>
    <table class="table table-striped">
      <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Date</th>
            <th class="act">Actions</th>
          </tr>
      </thead>
      <tbody>
        <?php foreach($this->content as $content){
          ?>
        <tr>
          <td><a href="<?php echo($this->generateURL("/orders/".($content->getID())."/show")); ?>"><?php echo($content->getID()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/orders/".($content->getID())."/show")); ?>"><?php echo($content->getUser()->getName()); ?></a></td>
          <td><a href="<?php echo($this->generateURL("/orders/".($content->getID())."/show")); ?>"><?php echo($content->getDate()->format("d-m-Y H:i:s")); ?></a></td>
          <td>
            <a href="<?php echo($this->generateURL("/orders/".($content->getID())."/show")); ?>" class="btn-sm btn-primary"><span class="glyphicon glyphicon-eye-open"></span> Details</a>
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>

    <?php $this->includePartial("home", "pagination"); ?>
  </div>
</div>
