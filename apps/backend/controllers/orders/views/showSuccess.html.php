<div class="row">

  <?php $this->includePartial("home", "messages"); ?>


  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3>Order: <?php echo($this->content->getID()); ?> - <?php echo($this->content->getDate()->format("d-m-Y H:i:s")); ?></h3>
        </div>
        <div class="panel-body">
          <p>
            <?php $user = $this->content->getUser(); ?>
            <strong>User: </strong> <a href="/users/<?php echo($user->getID()); ?>/edit"><?php echo($user->getName()); ?></a>
          </p>
          <p><a href="/users/<?php echo($user->getID()); ?>/orders" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open"></span> User Orders</a></p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Billing Address</h5>
        </div>
        <div class="panel-body">
          <div>
            <?php $this->address = $this->content->getBillingAddress(); ?>
            <?php $this->includePartial("home", "address"); ?>
          </div>

        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Shipping Address</h5>
        </div>
        <div class="panel-body">
          <div>
            <?php
            $this->address = $this->content->getAddress(); ?>
            <?php $this->includePartial("home", "address"); ?>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Products</h5>
        </div>
        <div class="panel-body">
          <table class="table table-striped">
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Quantity</th>
                  <th>Unity Price</th>
                  <th>Subtotal</th>
                </tr>
              </thead>
              <tbody>
                <?php $list = $this->content->getOrderLists(); ?>
                <?php
                  $listTotal = 0;
                  foreach($list as $item){
                    $product = $item->getProduct();
                    $itemPrice = $product->getPriceInCurrency($this->currencyFormat) * $item->getQuantity();
                    $listTotal += $itemPrice;
                ?>
                <tr>
                  <td><?php echo($product->getTitle()); ?></td>
                  <td><?php echo($item->getQuantity()); ?></td>
                  <td><?php echo($product->formatCurrency($this->currencyFormat)); ?></td>
                  <td><?php echo($product->formatCurrency($this->currencyFormat, $itemPrice)); ?></td>
                </tr>
                <?php } ?>
              </tbody>
              <tfoot>
                <?php
                  $total = $this->content->getTotal($this->currencyFormat);
                  $sub = $this->content->getSubTotal($this->currencyFormat);
                ?>

                <tr>
                  <td colspan="3">Sub Total:</td>
                  <td><?php echo($this->formatCurrency($this->currencyFormat, $sub)); ?></td>
                </tr>
                <?php if($this->content->getCouponId()){ ?>
                  <tr>
                    <td class="red" colspan="2">Discount Coupon:</td>
                    <td>-<?php echo($this->content->getCoupon()->getHumamReadable($this->currencyFormat)); ?></td>
                    <td class="red">-<?php echo($this->formatCurrency($this->currencyFormat, ($sub-$total))); ?></td>
                  </tr>
                <?php } ?>
                <tr>
                  <td colspan="3"><strong>Total:</strong></td>
                  <td><strong><?php echo($this->formatCurrency($this->currencyFormat, $total)); ?></strong></td>
                </tr>
              </tfoot>
          </table>
          <?php ; ?>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5>Order Status</h5>
        </div>
        <div class="panel-body">
          <div class="btn-group" role="group" aria-label="...">
            <?php
            $status = $this->content->getStatus();
            $steps = $this->content->getSteps();
            foreach($steps as $i => $step){ ?>
              <a href="/orders/<?php echo($this->content->getID()); ?>/<?php echo($this->Slugfy($step)); ?>" class="btn btn-default <?php echo(($i == $status)?"btn-primary disabled active":(($i < $status)?"disabled":"")); ?>"><?php echo($step);?></a>
            <?php
              }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
