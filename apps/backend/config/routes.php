<?php
TestShop\Dispatcher::addRoute(new TestShop\Routes\RouteAction("/^\/(login|logout)/i", "auth", ":1"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\SecureRouteAction("/^\/(orders)\/([0-9]+)\/(on-hold|approved-payment|order-shipped|closed)/i", "orders", "changeStatus", array("ID" => ":2", "status" => ":3")));
TestShop\Dispatcher::addRoute(new TestShop\Routes\SecureRouteAction("/^\/([a-z0-9]+)\/([0-9]+)\/(edit|delete|show)/i", ":1", ":3", array("ID" => ":2")));
TestShop\Dispatcher::addRoute(new TestShop\Routes\SecureRouteAction("/^\/([a-z0-9]+)\/([0-9]+)\/(orders|addresses)/i", ":3", "index", array("USER_ID" => ":2")));
TestShop\Dispatcher::addRoute(new TestShop\Routes\SecureRouteAction("/^\/([a-z0-9]+)\/(new)/i", ":1", "create"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\SecureRouteAction("/^\/([a-z0-9]+)\/([a-z0-9_-]+)\.(php|html)/i", ":1", ":2"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\SecureRouteAction("/^\/([a-z0-9_]+)/i", ":1", "index"));
TestShop\Dispatcher::addRoute(new TestShop\Routes\SecureRouteAction("/(\/index\.html)?/i", "home", "index"));

?>
