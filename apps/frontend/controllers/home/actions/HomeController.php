<?php
  namespace FrontEnd\Controllers;
  require_once("lib/DefaultController.php");
  use TestShop\Controllers as Controllers;

  class HomeController extends Controllers\DefaultController {
    public function index(){
      //return $this->callNextRoute();
    return $this->render("index");
    }
  }
?>
