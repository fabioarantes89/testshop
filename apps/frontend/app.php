<?php
  namespace FrontEnd;
  include_once("./lib/Application.php");
  use TestShop as TS;
  use TestShop\Routes as Routes;




  class Application extends TS\Application {
    static function init(): TS\Application {
      if(!empty(self::$app)){
        return self::$app;
      }
      self::$app = new Application("frontend");
      return self::$app;
    }
    public function hasController(String $controllerName, TS\Routes\RouteAction $route): Bool {
      $path = "apps/".$this->getName()."/controllers/{$controllerName}/actions/".(ucfirst($controllerName))."Controller.php";
      if(file_exists($path)){
        include_once($path);
        $class = __NAMESPACE__."\Controllers\\".(ucfirst($controllerName))."Controller";
        return class_exists($class);
      }
      return false;
    }
    public function getController(String $controllerName, TS\Routes\RouteAction $route): TS\Controllers\DefaultController {
      if($this->hasController($controllerName, $route)){
        $class = __NAMESPACE__."\Controllers\\".(ucfirst($controllerName))."Controller";
        $controller = new $class($route);
        return $controller;
      }
      include_once("lib/ErrorController.php");
      $controller = new TS\Controllers\ErrorController($route);
      return $controller;
    }
    protected function configure(){
      parent::configure();

      $this->setConfig(UPLOADS_WEB_PREFIX, "/");
      $this->setConfig(DEFAULT_RESPONSE_FORMAT, "html");
      $this->setConfig(IS_PRIVATE_APP, false);
      $this->setConfig(APP_WEB_PREFIX, "/");
      $this->setConfig(APP_AUTH_URL, "/oauth/token");
      $this->setConfig(UPLOADS_PATH_PREFIX, ".".DIRECTORY_SEPARATOR);
      $this->setConfig(UPLOADS_DIR, "uploads" . DIRECTORY_SEPARATOR);

    }

  }


?>
