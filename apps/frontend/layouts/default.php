<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>TestShop</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <link rel="stylesheet" href="/css/front.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
  <script src="https://code.angularjs.org/1.5.7/angular-resource.min.js"></script>
  <script src="/js/angular-route.js"></script>
  <script src="/js/ui-bootstrap-tpls-1.3.3.min.js"></script>
  <script src="https://code.angularjs.org/1.5.7/angular-sanitize.min.js"></script>
  <script src="https://code.angularjs.org/1.5.7/angular-touch.min.js"></script>
  <script src="/js/routes.js"></script>
  <script src="/js/angular-local-storage.js"></script>

  <script src="/js/testShop.js"></script>
</head>
<body ng-app="TestShop">
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/#/">TestShop</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/#/">Home</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">

            <li><a href="/#/cart">Shopping Cart <span class="badge" ng-if="cart">{{cart.length}}</span></a></li>
            <li ng-show="userData" class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{userData.Name}} <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/#/orders">Orders</a></li>
                <li><a href="/#/addresses">Addresses</a></li>
                <li><a href="/#/logout">Logout</a></li>
              </ul>
            </li>
            <li ng-hide="userData"><a href="/#/login">Login</a></li>
          </ul>

        </div><!--/.nav-collapse -->


        <current-cart></current-cart>
      </div>
    </nav>

    <div class="container">

      <div id="content" ng-view="">

      </div>

    </div><!-- /.container -->


</body>
</html>
