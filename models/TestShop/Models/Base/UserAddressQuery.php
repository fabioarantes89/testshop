<?php

namespace TestShop\Models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use TestShop\Models\UserAddress as ChildUserAddress;
use TestShop\Models\UserAddressQuery as ChildUserAddressQuery;
use TestShop\Models\Map\UserAddressTableMap;

/**
 * Base class that represents a query for the 'address' table.
 *
 *
 *
 * @method     ChildUserAddressQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserAddressQuery orderByFullName($order = Criteria::ASC) Order by the full_name column
 * @method     ChildUserAddressQuery orderByAddressLine($order = Criteria::ASC) Order by the address_line column
 * @method     ChildUserAddressQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method     ChildUserAddressQuery orderByProvince($order = Criteria::ASC) Order by the province column
 * @method     ChildUserAddressQuery orderByCountry($order = Criteria::ASC) Order by the country column
 * @method     ChildUserAddressQuery orderByZipCode($order = Criteria::ASC) Order by the zip_code column
 * @method     ChildUserAddressQuery orderByTelephone($order = Criteria::ASC) Order by the telephone column
 * @method     ChildUserAddressQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildUserAddressQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildUserAddressQuery groupById() Group by the id column
 * @method     ChildUserAddressQuery groupByFullName() Group by the full_name column
 * @method     ChildUserAddressQuery groupByAddressLine() Group by the address_line column
 * @method     ChildUserAddressQuery groupByCity() Group by the city column
 * @method     ChildUserAddressQuery groupByProvince() Group by the province column
 * @method     ChildUserAddressQuery groupByCountry() Group by the country column
 * @method     ChildUserAddressQuery groupByZipCode() Group by the zip_code column
 * @method     ChildUserAddressQuery groupByTelephone() Group by the telephone column
 * @method     ChildUserAddressQuery groupByUserId() Group by the user_id column
 * @method     ChildUserAddressQuery groupByStatus() Group by the status column
 *
 * @method     ChildUserAddressQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserAddressQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserAddressQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserAddressQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserAddressQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserAddressQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserAddressQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildUserAddressQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildUserAddressQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildUserAddressQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildUserAddressQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildUserAddressQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildUserAddressQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildUserAddressQuery leftJoinOrderRelatedByAddressId($relationAlias = null) Adds a LEFT JOIN clause to the query using the OrderRelatedByAddressId relation
 * @method     ChildUserAddressQuery rightJoinOrderRelatedByAddressId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OrderRelatedByAddressId relation
 * @method     ChildUserAddressQuery innerJoinOrderRelatedByAddressId($relationAlias = null) Adds a INNER JOIN clause to the query using the OrderRelatedByAddressId relation
 *
 * @method     ChildUserAddressQuery joinWithOrderRelatedByAddressId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OrderRelatedByAddressId relation
 *
 * @method     ChildUserAddressQuery leftJoinWithOrderRelatedByAddressId() Adds a LEFT JOIN clause and with to the query using the OrderRelatedByAddressId relation
 * @method     ChildUserAddressQuery rightJoinWithOrderRelatedByAddressId() Adds a RIGHT JOIN clause and with to the query using the OrderRelatedByAddressId relation
 * @method     ChildUserAddressQuery innerJoinWithOrderRelatedByAddressId() Adds a INNER JOIN clause and with to the query using the OrderRelatedByAddressId relation
 *
 * @method     ChildUserAddressQuery leftJoinOrderRelatedByBillingAddressId($relationAlias = null) Adds a LEFT JOIN clause to the query using the OrderRelatedByBillingAddressId relation
 * @method     ChildUserAddressQuery rightJoinOrderRelatedByBillingAddressId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OrderRelatedByBillingAddressId relation
 * @method     ChildUserAddressQuery innerJoinOrderRelatedByBillingAddressId($relationAlias = null) Adds a INNER JOIN clause to the query using the OrderRelatedByBillingAddressId relation
 *
 * @method     ChildUserAddressQuery joinWithOrderRelatedByBillingAddressId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OrderRelatedByBillingAddressId relation
 *
 * @method     ChildUserAddressQuery leftJoinWithOrderRelatedByBillingAddressId() Adds a LEFT JOIN clause and with to the query using the OrderRelatedByBillingAddressId relation
 * @method     ChildUserAddressQuery rightJoinWithOrderRelatedByBillingAddressId() Adds a RIGHT JOIN clause and with to the query using the OrderRelatedByBillingAddressId relation
 * @method     ChildUserAddressQuery innerJoinWithOrderRelatedByBillingAddressId() Adds a INNER JOIN clause and with to the query using the OrderRelatedByBillingAddressId relation
 *
 * @method     \TestShop\Models\UserQuery|\TestShop\Models\OrderQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUserAddress findOne(ConnectionInterface $con = null) Return the first ChildUserAddress matching the query
 * @method     ChildUserAddress findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserAddress matching the query, or a new ChildUserAddress object populated from the query conditions when no match is found
 *
 * @method     ChildUserAddress findOneById(int $id) Return the first ChildUserAddress filtered by the id column
 * @method     ChildUserAddress findOneByFullName(string $full_name) Return the first ChildUserAddress filtered by the full_name column
 * @method     ChildUserAddress findOneByAddressLine(string $address_line) Return the first ChildUserAddress filtered by the address_line column
 * @method     ChildUserAddress findOneByCity(string $city) Return the first ChildUserAddress filtered by the city column
 * @method     ChildUserAddress findOneByProvince(string $province) Return the first ChildUserAddress filtered by the province column
 * @method     ChildUserAddress findOneByCountry(string $country) Return the first ChildUserAddress filtered by the country column
 * @method     ChildUserAddress findOneByZipCode(string $zip_code) Return the first ChildUserAddress filtered by the zip_code column
 * @method     ChildUserAddress findOneByTelephone(string $telephone) Return the first ChildUserAddress filtered by the telephone column
 * @method     ChildUserAddress findOneByUserId(int $user_id) Return the first ChildUserAddress filtered by the user_id column
 * @method     ChildUserAddress findOneByStatus(int $status) Return the first ChildUserAddress filtered by the status column *

 * @method     ChildUserAddress requirePk($key, ConnectionInterface $con = null) Return the ChildUserAddress by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOne(ConnectionInterface $con = null) Return the first ChildUserAddress matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserAddress requireOneById(int $id) Return the first ChildUserAddress filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByFullName(string $full_name) Return the first ChildUserAddress filtered by the full_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByAddressLine(string $address_line) Return the first ChildUserAddress filtered by the address_line column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByCity(string $city) Return the first ChildUserAddress filtered by the city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByProvince(string $province) Return the first ChildUserAddress filtered by the province column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByCountry(string $country) Return the first ChildUserAddress filtered by the country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByZipCode(string $zip_code) Return the first ChildUserAddress filtered by the zip_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByTelephone(string $telephone) Return the first ChildUserAddress filtered by the telephone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByUserId(int $user_id) Return the first ChildUserAddress filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAddress requireOneByStatus(int $status) Return the first ChildUserAddress filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserAddress[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserAddress objects based on current ModelCriteria
 * @method     ChildUserAddress[]|ObjectCollection findById(int $id) Return ChildUserAddress objects filtered by the id column
 * @method     ChildUserAddress[]|ObjectCollection findByFullName(string $full_name) Return ChildUserAddress objects filtered by the full_name column
 * @method     ChildUserAddress[]|ObjectCollection findByAddressLine(string $address_line) Return ChildUserAddress objects filtered by the address_line column
 * @method     ChildUserAddress[]|ObjectCollection findByCity(string $city) Return ChildUserAddress objects filtered by the city column
 * @method     ChildUserAddress[]|ObjectCollection findByProvince(string $province) Return ChildUserAddress objects filtered by the province column
 * @method     ChildUserAddress[]|ObjectCollection findByCountry(string $country) Return ChildUserAddress objects filtered by the country column
 * @method     ChildUserAddress[]|ObjectCollection findByZipCode(string $zip_code) Return ChildUserAddress objects filtered by the zip_code column
 * @method     ChildUserAddress[]|ObjectCollection findByTelephone(string $telephone) Return ChildUserAddress objects filtered by the telephone column
 * @method     ChildUserAddress[]|ObjectCollection findByUserId(int $user_id) Return ChildUserAddress objects filtered by the user_id column
 * @method     ChildUserAddress[]|ObjectCollection findByStatus(int $status) Return ChildUserAddress objects filtered by the status column
 * @method     ChildUserAddress[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserAddressQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \TestShop\Models\Base\UserAddressQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\TestShop\\Models\\UserAddress', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserAddressQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserAddressQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserAddressQuery) {
            return $criteria;
        }
        $query = new ChildUserAddressQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserAddress|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserAddressTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserAddressTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserAddress A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, full_name, address_line, city, province, country, zip_code, telephone, user_id, status FROM address WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserAddress $obj */
            $obj = new ChildUserAddress();
            $obj->hydrate($row);
            UserAddressTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserAddress|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserAddressTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserAddressTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserAddressTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserAddressTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the full_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFullName('fooValue');   // WHERE full_name = 'fooValue'
     * $query->filterByFullName('%fooValue%'); // WHERE full_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fullName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByFullName($fullName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fullName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_FULL_NAME, $fullName, $comparison);
    }

    /**
     * Filter the query on the address_line column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressLine('fooValue');   // WHERE address_line = 'fooValue'
     * $query->filterByAddressLine('%fooValue%'); // WHERE address_line LIKE '%fooValue%'
     * </code>
     *
     * @param     string $addressLine The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByAddressLine($addressLine = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($addressLine)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_ADDRESS_LINE, $addressLine, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%'); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_CITY, $city, $comparison);
    }

    /**
     * Filter the query on the province column
     *
     * Example usage:
     * <code>
     * $query->filterByProvince('fooValue');   // WHERE province = 'fooValue'
     * $query->filterByProvince('%fooValue%'); // WHERE province LIKE '%fooValue%'
     * </code>
     *
     * @param     string $province The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByProvince($province = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($province)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_PROVINCE, $province, $comparison);
    }

    /**
     * Filter the query on the country column
     *
     * Example usage:
     * <code>
     * $query->filterByCountry('fooValue');   // WHERE country = 'fooValue'
     * $query->filterByCountry('%fooValue%'); // WHERE country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $country The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByCountry($country = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($country)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_COUNTRY, $country, $comparison);
    }

    /**
     * Filter the query on the zip_code column
     *
     * Example usage:
     * <code>
     * $query->filterByZipCode('fooValue');   // WHERE zip_code = 'fooValue'
     * $query->filterByZipCode('%fooValue%'); // WHERE zip_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zipCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByZipCode($zipCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zipCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_ZIP_CODE, $zipCode, $comparison);
    }

    /**
     * Filter the query on the telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephone('fooValue');   // WHERE telephone = 'fooValue'
     * $query->filterByTelephone('%fooValue%'); // WHERE telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByTelephone($telephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_TELEPHONE, $telephone, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(UserAddressTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(UserAddressTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(UserAddressTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(UserAddressTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAddressTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query by a related \TestShop\Models\User object
     *
     * @param \TestShop\Models\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \TestShop\Models\User) {
            return $this
                ->addUsingAlias(UserAddressTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserAddressTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \TestShop\Models\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TestShop\Models\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\TestShop\Models\UserQuery');
    }

    /**
     * Filter the query by a related \TestShop\Models\Order object
     *
     * @param \TestShop\Models\Order|ObjectCollection $order the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByOrderRelatedByAddressId($order, $comparison = null)
    {
        if ($order instanceof \TestShop\Models\Order) {
            return $this
                ->addUsingAlias(UserAddressTableMap::COL_ID, $order->getAddressId(), $comparison);
        } elseif ($order instanceof ObjectCollection) {
            return $this
                ->useOrderRelatedByAddressIdQuery()
                ->filterByPrimaryKeys($order->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOrderRelatedByAddressId() only accepts arguments of type \TestShop\Models\Order or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OrderRelatedByAddressId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function joinOrderRelatedByAddressId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OrderRelatedByAddressId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OrderRelatedByAddressId');
        }

        return $this;
    }

    /**
     * Use the OrderRelatedByAddressId relation Order object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TestShop\Models\OrderQuery A secondary query class using the current class as primary query
     */
    public function useOrderRelatedByAddressIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOrderRelatedByAddressId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OrderRelatedByAddressId', '\TestShop\Models\OrderQuery');
    }

    /**
     * Filter the query by a related \TestShop\Models\Order object
     *
     * @param \TestShop\Models\Order|ObjectCollection $order the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserAddressQuery The current query, for fluid interface
     */
    public function filterByOrderRelatedByBillingAddressId($order, $comparison = null)
    {
        if ($order instanceof \TestShop\Models\Order) {
            return $this
                ->addUsingAlias(UserAddressTableMap::COL_ID, $order->getBillingAddressId(), $comparison);
        } elseif ($order instanceof ObjectCollection) {
            return $this
                ->useOrderRelatedByBillingAddressIdQuery()
                ->filterByPrimaryKeys($order->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOrderRelatedByBillingAddressId() only accepts arguments of type \TestShop\Models\Order or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OrderRelatedByBillingAddressId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function joinOrderRelatedByBillingAddressId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OrderRelatedByBillingAddressId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OrderRelatedByBillingAddressId');
        }

        return $this;
    }

    /**
     * Use the OrderRelatedByBillingAddressId relation Order object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TestShop\Models\OrderQuery A secondary query class using the current class as primary query
     */
    public function useOrderRelatedByBillingAddressIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOrderRelatedByBillingAddressId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OrderRelatedByBillingAddressId', '\TestShop\Models\OrderQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserAddress $userAddress Object to remove from the list of results
     *
     * @return $this|ChildUserAddressQuery The current query, for fluid interface
     */
    public function prune($userAddress = null)
    {
        if ($userAddress) {
            $this->addUsingAlias(UserAddressTableMap::COL_ID, $userAddress->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the address table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAddressTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserAddressTableMap::clearInstancePool();
            UserAddressTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAddressTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserAddressTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserAddressTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserAddressTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserAddressQuery
