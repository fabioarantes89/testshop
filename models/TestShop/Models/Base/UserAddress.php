<?php

namespace TestShop\Models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Symfony\Component\Translation\IdentityTranslator;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Context\ExecutionContextFactory;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Mapping\Loader\StaticMethodLoader;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TestShop\Models\Order as ChildOrder;
use TestShop\Models\OrderQuery as ChildOrderQuery;
use TestShop\Models\User as ChildUser;
use TestShop\Models\UserAddress as ChildUserAddress;
use TestShop\Models\UserAddressQuery as ChildUserAddressQuery;
use TestShop\Models\UserQuery as ChildUserQuery;
use TestShop\Models\Map\OrderTableMap;
use TestShop\Models\Map\UserAddressTableMap;

/**
 * Base class that represents a row from the 'address' table.
 *
 *
 *
 * @package    propel.generator.TestShop.Models.Base
 */
abstract class UserAddress implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\TestShop\\Models\\Map\\UserAddressTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the full_name field.
     *
     * @var        string
     */
    protected $full_name;

    /**
     * The value for the address_line field.
     *
     * @var        string
     */
    protected $address_line;

    /**
     * The value for the city field.
     *
     * @var        string
     */
    protected $city;

    /**
     * The value for the province field.
     *
     * @var        string
     */
    protected $province;

    /**
     * The value for the country field.
     *
     * @var        string
     */
    protected $country;

    /**
     * The value for the zip_code field.
     *
     * @var        string
     */
    protected $zip_code;

    /**
     * The value for the telephone field.
     *
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the user_id field.
     *
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the status field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $status;

    /**
     * @var        ChildUser
     */
    protected $aUser;

    /**
     * @var        ObjectCollection|ChildOrder[] Collection to store aggregation of ChildOrder objects.
     */
    protected $collOrdersRelatedByAddressId;
    protected $collOrdersRelatedByAddressIdPartial;

    /**
     * @var        ObjectCollection|ChildOrder[] Collection to store aggregation of ChildOrder objects.
     */
    protected $collOrdersRelatedByBillingAddressId;
    protected $collOrdersRelatedByBillingAddressIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // validate behavior

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * ConstraintViolationList object
     *
     * @see     http://api.symfony.com/2.0/Symfony/Component/Validator/ConstraintViolationList.html
     * @var     ConstraintViolationList
     */
    protected $validationFailures;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOrder[]
     */
    protected $ordersRelatedByAddressIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOrder[]
     */
    protected $ordersRelatedByBillingAddressIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->status = 1;
    }

    /**
     * Initializes internal state of TestShop\Models\Base\UserAddress object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>UserAddress</code> instance.  If
     * <code>obj</code> is an instance of <code>UserAddress</code>, delegates to
     * <code>equals(UserAddress)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|UserAddress The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [full_name] column value.
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * Get the [address_line] column value.
     *
     * @return string
     */
    public function getAddressLine()
    {
        return $this->address_line;
    }

    /**
     * Get the [city] column value.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Get the [province] column value.
     *
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Get the [country] column value.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get the [zip_code] column value.
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Get the [status] column value.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [full_name] column.
     *
     * @param string $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setFullName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->full_name !== $v) {
            $this->full_name = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_FULL_NAME] = true;
        }

        return $this;
    } // setFullName()

    /**
     * Set the value of [address_line] column.
     *
     * @param string $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setAddressLine($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->address_line !== $v) {
            $this->address_line = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_ADDRESS_LINE] = true;
        }

        return $this;
    } // setAddressLine()

    /**
     * Set the value of [city] column.
     *
     * @param string $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->city !== $v) {
            $this->city = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_CITY] = true;
        }

        return $this;
    } // setCity()

    /**
     * Set the value of [province] column.
     *
     * @param string $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setProvince($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->province !== $v) {
            $this->province = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_PROVINCE] = true;
        }

        return $this;
    } // setProvince()

    /**
     * Set the value of [country] column.
     *
     * @param string $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->country !== $v) {
            $this->country = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_COUNTRY] = true;
        }

        return $this;
    } // setCountry()

    /**
     * Set the value of [zip_code] column.
     *
     * @param string $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setZipCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->zip_code !== $v) {
            $this->zip_code = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_ZIP_CODE] = true;
        }

        return $this;
    } // setZipCode()

    /**
     * Set the value of [telephone] column.
     *
     * @param string $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_TELEPHONE] = true;
        }

        return $this;
    } // setTelephone()

    /**
     * Set the value of [user_id] column.
     *
     * @param int $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_USER_ID] = true;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }

        return $this;
    } // setUserId()

    /**
     * Set the value of [status] column.
     *
     * @param int $v new value
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[UserAddressTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->status !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UserAddressTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UserAddressTableMap::translateFieldName('FullName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->full_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UserAddressTableMap::translateFieldName('AddressLine', TableMap::TYPE_PHPNAME, $indexType)];
            $this->address_line = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UserAddressTableMap::translateFieldName('City', TableMap::TYPE_PHPNAME, $indexType)];
            $this->city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UserAddressTableMap::translateFieldName('Province', TableMap::TYPE_PHPNAME, $indexType)];
            $this->province = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UserAddressTableMap::translateFieldName('Country', TableMap::TYPE_PHPNAME, $indexType)];
            $this->country = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UserAddressTableMap::translateFieldName('ZipCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->zip_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UserAddressTableMap::translateFieldName('Telephone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->telephone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UserAddressTableMap::translateFieldName('UserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UserAddressTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = UserAddressTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\TestShop\\Models\\UserAddress'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUser !== null && $this->user_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserAddressTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUserAddressQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aUser = null;
            $this->collOrdersRelatedByAddressId = null;

            $this->collOrdersRelatedByBillingAddressId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see UserAddress::setDeleted()
     * @see UserAddress::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAddressTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUserAddressQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAddressTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserAddressTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->ordersRelatedByAddressIdScheduledForDeletion !== null) {
                if (!$this->ordersRelatedByAddressIdScheduledForDeletion->isEmpty()) {
                    \TestShop\Models\OrderQuery::create()
                        ->filterByPrimaryKeys($this->ordersRelatedByAddressIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ordersRelatedByAddressIdScheduledForDeletion = null;
                }
            }

            if ($this->collOrdersRelatedByAddressId !== null) {
                foreach ($this->collOrdersRelatedByAddressId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ordersRelatedByBillingAddressIdScheduledForDeletion !== null) {
                if (!$this->ordersRelatedByBillingAddressIdScheduledForDeletion->isEmpty()) {
                    \TestShop\Models\OrderQuery::create()
                        ->filterByPrimaryKeys($this->ordersRelatedByBillingAddressIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ordersRelatedByBillingAddressIdScheduledForDeletion = null;
                }
            }

            if ($this->collOrdersRelatedByBillingAddressId !== null) {
                foreach ($this->collOrdersRelatedByBillingAddressId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UserAddressTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserAddressTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserAddressTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_FULL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'full_name';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_ADDRESS_LINE)) {
            $modifiedColumns[':p' . $index++]  = 'address_line';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'city';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_PROVINCE)) {
            $modifiedColumns[':p' . $index++]  = 'province';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'country';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_ZIP_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'zip_code';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = 'telephone';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'user_id';
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }

        $sql = sprintf(
            'INSERT INTO address (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'full_name':
                        $stmt->bindValue($identifier, $this->full_name, PDO::PARAM_STR);
                        break;
                    case 'address_line':
                        $stmt->bindValue($identifier, $this->address_line, PDO::PARAM_STR);
                        break;
                    case 'city':
                        $stmt->bindValue($identifier, $this->city, PDO::PARAM_STR);
                        break;
                    case 'province':
                        $stmt->bindValue($identifier, $this->province, PDO::PARAM_STR);
                        break;
                    case 'country':
                        $stmt->bindValue($identifier, $this->country, PDO::PARAM_STR);
                        break;
                    case 'zip_code':
                        $stmt->bindValue($identifier, $this->zip_code, PDO::PARAM_STR);
                        break;
                    case 'telephone':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case 'user_id':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserAddressTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getFullName();
                break;
            case 2:
                return $this->getAddressLine();
                break;
            case 3:
                return $this->getCity();
                break;
            case 4:
                return $this->getProvince();
                break;
            case 5:
                return $this->getCountry();
                break;
            case 6:
                return $this->getZipCode();
                break;
            case 7:
                return $this->getTelephone();
                break;
            case 8:
                return $this->getUserId();
                break;
            case 9:
                return $this->getStatus();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['UserAddress'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['UserAddress'][$this->hashCode()] = true;
        $keys = UserAddressTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getFullName(),
            $keys[2] => $this->getAddressLine(),
            $keys[3] => $this->getCity(),
            $keys[4] => $this->getProvince(),
            $keys[5] => $this->getCountry(),
            $keys[6] => $this->getZipCode(),
            $keys[7] => $this->getTelephone(),
            $keys[8] => $this->getUserId(),
            $keys[9] => $this->getStatus(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user';
                        break;
                    default:
                        $key = 'User';
                }

                $result[$key] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collOrdersRelatedByAddressId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'orders';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'orderss';
                        break;
                    default:
                        $key = 'Orders';
                }

                $result[$key] = $this->collOrdersRelatedByAddressId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collOrdersRelatedByBillingAddressId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'orders';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'orderss';
                        break;
                    default:
                        $key = 'Orders';
                }

                $result[$key] = $this->collOrdersRelatedByBillingAddressId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\TestShop\Models\UserAddress
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserAddressTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\TestShop\Models\UserAddress
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setFullName($value);
                break;
            case 2:
                $this->setAddressLine($value);
                break;
            case 3:
                $this->setCity($value);
                break;
            case 4:
                $this->setProvince($value);
                break;
            case 5:
                $this->setCountry($value);
                break;
            case 6:
                $this->setZipCode($value);
                break;
            case 7:
                $this->setTelephone($value);
                break;
            case 8:
                $this->setUserId($value);
                break;
            case 9:
                $this->setStatus($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UserAddressTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setFullName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAddressLine($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCity($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setProvince($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCountry($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setZipCode($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTelephone($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setUserId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setStatus($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\TestShop\Models\UserAddress The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserAddressTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UserAddressTableMap::COL_ID)) {
            $criteria->add(UserAddressTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_FULL_NAME)) {
            $criteria->add(UserAddressTableMap::COL_FULL_NAME, $this->full_name);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_ADDRESS_LINE)) {
            $criteria->add(UserAddressTableMap::COL_ADDRESS_LINE, $this->address_line);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_CITY)) {
            $criteria->add(UserAddressTableMap::COL_CITY, $this->city);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_PROVINCE)) {
            $criteria->add(UserAddressTableMap::COL_PROVINCE, $this->province);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_COUNTRY)) {
            $criteria->add(UserAddressTableMap::COL_COUNTRY, $this->country);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_ZIP_CODE)) {
            $criteria->add(UserAddressTableMap::COL_ZIP_CODE, $this->zip_code);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_TELEPHONE)) {
            $criteria->add(UserAddressTableMap::COL_TELEPHONE, $this->telephone);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_USER_ID)) {
            $criteria->add(UserAddressTableMap::COL_USER_ID, $this->user_id);
        }
        if ($this->isColumnModified(UserAddressTableMap::COL_STATUS)) {
            $criteria->add(UserAddressTableMap::COL_STATUS, $this->status);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUserAddressQuery::create();
        $criteria->add(UserAddressTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \TestShop\Models\UserAddress (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setFullName($this->getFullName());
        $copyObj->setAddressLine($this->getAddressLine());
        $copyObj->setCity($this->getCity());
        $copyObj->setProvince($this->getProvince());
        $copyObj->setCountry($this->getCountry());
        $copyObj->setZipCode($this->getZipCode());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setUserId($this->getUserId());
        $copyObj->setStatus($this->getStatus());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getOrdersRelatedByAddressId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOrderRelatedByAddressId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getOrdersRelatedByBillingAddressId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOrderRelatedByBillingAddressId($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \TestShop\Models\UserAddress Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildUser object.
     *
     * @param  ChildUser $v
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(ChildUser $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUser object, it will not be re-added.
        if ($v !== null) {
            $v->addUserAddress($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUser object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUser The associated ChildUser object.
     * @throws PropelException
     */
    public function getUser(ConnectionInterface $con = null)
    {
        if ($this->aUser === null && ($this->user_id !== null)) {
            $this->aUser = ChildUserQuery::create()->findPk($this->user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addUserAddresses($this);
             */
        }

        return $this->aUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('OrderRelatedByAddressId' == $relationName) {
            return $this->initOrdersRelatedByAddressId();
        }
        if ('OrderRelatedByBillingAddressId' == $relationName) {
            return $this->initOrdersRelatedByBillingAddressId();
        }
    }

    /**
     * Clears out the collOrdersRelatedByAddressId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOrdersRelatedByAddressId()
     */
    public function clearOrdersRelatedByAddressId()
    {
        $this->collOrdersRelatedByAddressId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOrdersRelatedByAddressId collection loaded partially.
     */
    public function resetPartialOrdersRelatedByAddressId($v = true)
    {
        $this->collOrdersRelatedByAddressIdPartial = $v;
    }

    /**
     * Initializes the collOrdersRelatedByAddressId collection.
     *
     * By default this just sets the collOrdersRelatedByAddressId collection to an empty array (like clearcollOrdersRelatedByAddressId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOrdersRelatedByAddressId($overrideExisting = true)
    {
        if (null !== $this->collOrdersRelatedByAddressId && !$overrideExisting) {
            return;
        }

        $collectionClassName = OrderTableMap::getTableMap()->getCollectionClassName();

        $this->collOrdersRelatedByAddressId = new $collectionClassName;
        $this->collOrdersRelatedByAddressId->setModel('\TestShop\Models\Order');
    }

    /**
     * Gets an array of ChildOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserAddress is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOrder[] List of ChildOrder objects
     * @throws PropelException
     */
    public function getOrdersRelatedByAddressId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOrdersRelatedByAddressIdPartial && !$this->isNew();
        if (null === $this->collOrdersRelatedByAddressId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOrdersRelatedByAddressId) {
                // return empty collection
                $this->initOrdersRelatedByAddressId();
            } else {
                $collOrdersRelatedByAddressId = ChildOrderQuery::create(null, $criteria)
                    ->filterByaddress($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOrdersRelatedByAddressIdPartial && count($collOrdersRelatedByAddressId)) {
                        $this->initOrdersRelatedByAddressId(false);

                        foreach ($collOrdersRelatedByAddressId as $obj) {
                            if (false == $this->collOrdersRelatedByAddressId->contains($obj)) {
                                $this->collOrdersRelatedByAddressId->append($obj);
                            }
                        }

                        $this->collOrdersRelatedByAddressIdPartial = true;
                    }

                    return $collOrdersRelatedByAddressId;
                }

                if ($partial && $this->collOrdersRelatedByAddressId) {
                    foreach ($this->collOrdersRelatedByAddressId as $obj) {
                        if ($obj->isNew()) {
                            $collOrdersRelatedByAddressId[] = $obj;
                        }
                    }
                }

                $this->collOrdersRelatedByAddressId = $collOrdersRelatedByAddressId;
                $this->collOrdersRelatedByAddressIdPartial = false;
            }
        }

        return $this->collOrdersRelatedByAddressId;
    }

    /**
     * Sets a collection of ChildOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ordersRelatedByAddressId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserAddress The current object (for fluent API support)
     */
    public function setOrdersRelatedByAddressId(Collection $ordersRelatedByAddressId, ConnectionInterface $con = null)
    {
        /** @var ChildOrder[] $ordersRelatedByAddressIdToDelete */
        $ordersRelatedByAddressIdToDelete = $this->getOrdersRelatedByAddressId(new Criteria(), $con)->diff($ordersRelatedByAddressId);


        $this->ordersRelatedByAddressIdScheduledForDeletion = $ordersRelatedByAddressIdToDelete;

        foreach ($ordersRelatedByAddressIdToDelete as $orderRelatedByAddressIdRemoved) {
            $orderRelatedByAddressIdRemoved->setaddress(null);
        }

        $this->collOrdersRelatedByAddressId = null;
        foreach ($ordersRelatedByAddressId as $orderRelatedByAddressId) {
            $this->addOrderRelatedByAddressId($orderRelatedByAddressId);
        }

        $this->collOrdersRelatedByAddressId = $ordersRelatedByAddressId;
        $this->collOrdersRelatedByAddressIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Order objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Order objects.
     * @throws PropelException
     */
    public function countOrdersRelatedByAddressId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOrdersRelatedByAddressIdPartial && !$this->isNew();
        if (null === $this->collOrdersRelatedByAddressId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOrdersRelatedByAddressId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOrdersRelatedByAddressId());
            }

            $query = ChildOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByaddress($this)
                ->count($con);
        }

        return count($this->collOrdersRelatedByAddressId);
    }

    /**
     * Method called to associate a ChildOrder object to this object
     * through the ChildOrder foreign key attribute.
     *
     * @param  ChildOrder $l ChildOrder
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function addOrderRelatedByAddressId(ChildOrder $l)
    {
        if ($this->collOrdersRelatedByAddressId === null) {
            $this->initOrdersRelatedByAddressId();
            $this->collOrdersRelatedByAddressIdPartial = true;
        }

        if (!$this->collOrdersRelatedByAddressId->contains($l)) {
            $this->doAddOrderRelatedByAddressId($l);

            if ($this->ordersRelatedByAddressIdScheduledForDeletion and $this->ordersRelatedByAddressIdScheduledForDeletion->contains($l)) {
                $this->ordersRelatedByAddressIdScheduledForDeletion->remove($this->ordersRelatedByAddressIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOrder $orderRelatedByAddressId The ChildOrder object to add.
     */
    protected function doAddOrderRelatedByAddressId(ChildOrder $orderRelatedByAddressId)
    {
        $this->collOrdersRelatedByAddressId[]= $orderRelatedByAddressId;
        $orderRelatedByAddressId->setaddress($this);
    }

    /**
     * @param  ChildOrder $orderRelatedByAddressId The ChildOrder object to remove.
     * @return $this|ChildUserAddress The current object (for fluent API support)
     */
    public function removeOrderRelatedByAddressId(ChildOrder $orderRelatedByAddressId)
    {
        if ($this->getOrdersRelatedByAddressId()->contains($orderRelatedByAddressId)) {
            $pos = $this->collOrdersRelatedByAddressId->search($orderRelatedByAddressId);
            $this->collOrdersRelatedByAddressId->remove($pos);
            if (null === $this->ordersRelatedByAddressIdScheduledForDeletion) {
                $this->ordersRelatedByAddressIdScheduledForDeletion = clone $this->collOrdersRelatedByAddressId;
                $this->ordersRelatedByAddressIdScheduledForDeletion->clear();
            }
            $this->ordersRelatedByAddressIdScheduledForDeletion[]= clone $orderRelatedByAddressId;
            $orderRelatedByAddressId->setaddress(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserAddress is new, it will return
     * an empty collection; or if this UserAddress has previously
     * been saved, it will retrieve related OrdersRelatedByAddressId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserAddress.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOrder[] List of ChildOrder objects
     */
    public function getOrdersRelatedByAddressIdJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOrderQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getOrdersRelatedByAddressId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserAddress is new, it will return
     * an empty collection; or if this UserAddress has previously
     * been saved, it will retrieve related OrdersRelatedByAddressId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserAddress.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOrder[] List of ChildOrder objects
     */
    public function getOrdersRelatedByAddressIdJoinCoupon(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOrderQuery::create(null, $criteria);
        $query->joinWith('Coupon', $joinBehavior);

        return $this->getOrdersRelatedByAddressId($query, $con);
    }

    /**
     * Clears out the collOrdersRelatedByBillingAddressId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addOrdersRelatedByBillingAddressId()
     */
    public function clearOrdersRelatedByBillingAddressId()
    {
        $this->collOrdersRelatedByBillingAddressId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collOrdersRelatedByBillingAddressId collection loaded partially.
     */
    public function resetPartialOrdersRelatedByBillingAddressId($v = true)
    {
        $this->collOrdersRelatedByBillingAddressIdPartial = $v;
    }

    /**
     * Initializes the collOrdersRelatedByBillingAddressId collection.
     *
     * By default this just sets the collOrdersRelatedByBillingAddressId collection to an empty array (like clearcollOrdersRelatedByBillingAddressId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOrdersRelatedByBillingAddressId($overrideExisting = true)
    {
        if (null !== $this->collOrdersRelatedByBillingAddressId && !$overrideExisting) {
            return;
        }

        $collectionClassName = OrderTableMap::getTableMap()->getCollectionClassName();

        $this->collOrdersRelatedByBillingAddressId = new $collectionClassName;
        $this->collOrdersRelatedByBillingAddressId->setModel('\TestShop\Models\Order');
    }

    /**
     * Gets an array of ChildOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserAddress is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOrder[] List of ChildOrder objects
     * @throws PropelException
     */
    public function getOrdersRelatedByBillingAddressId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collOrdersRelatedByBillingAddressIdPartial && !$this->isNew();
        if (null === $this->collOrdersRelatedByBillingAddressId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOrdersRelatedByBillingAddressId) {
                // return empty collection
                $this->initOrdersRelatedByBillingAddressId();
            } else {
                $collOrdersRelatedByBillingAddressId = ChildOrderQuery::create(null, $criteria)
                    ->filterBybillingAddress($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collOrdersRelatedByBillingAddressIdPartial && count($collOrdersRelatedByBillingAddressId)) {
                        $this->initOrdersRelatedByBillingAddressId(false);

                        foreach ($collOrdersRelatedByBillingAddressId as $obj) {
                            if (false == $this->collOrdersRelatedByBillingAddressId->contains($obj)) {
                                $this->collOrdersRelatedByBillingAddressId->append($obj);
                            }
                        }

                        $this->collOrdersRelatedByBillingAddressIdPartial = true;
                    }

                    return $collOrdersRelatedByBillingAddressId;
                }

                if ($partial && $this->collOrdersRelatedByBillingAddressId) {
                    foreach ($this->collOrdersRelatedByBillingAddressId as $obj) {
                        if ($obj->isNew()) {
                            $collOrdersRelatedByBillingAddressId[] = $obj;
                        }
                    }
                }

                $this->collOrdersRelatedByBillingAddressId = $collOrdersRelatedByBillingAddressId;
                $this->collOrdersRelatedByBillingAddressIdPartial = false;
            }
        }

        return $this->collOrdersRelatedByBillingAddressId;
    }

    /**
     * Sets a collection of ChildOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ordersRelatedByBillingAddressId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserAddress The current object (for fluent API support)
     */
    public function setOrdersRelatedByBillingAddressId(Collection $ordersRelatedByBillingAddressId, ConnectionInterface $con = null)
    {
        /** @var ChildOrder[] $ordersRelatedByBillingAddressIdToDelete */
        $ordersRelatedByBillingAddressIdToDelete = $this->getOrdersRelatedByBillingAddressId(new Criteria(), $con)->diff($ordersRelatedByBillingAddressId);


        $this->ordersRelatedByBillingAddressIdScheduledForDeletion = $ordersRelatedByBillingAddressIdToDelete;

        foreach ($ordersRelatedByBillingAddressIdToDelete as $orderRelatedByBillingAddressIdRemoved) {
            $orderRelatedByBillingAddressIdRemoved->setbillingAddress(null);
        }

        $this->collOrdersRelatedByBillingAddressId = null;
        foreach ($ordersRelatedByBillingAddressId as $orderRelatedByBillingAddressId) {
            $this->addOrderRelatedByBillingAddressId($orderRelatedByBillingAddressId);
        }

        $this->collOrdersRelatedByBillingAddressId = $ordersRelatedByBillingAddressId;
        $this->collOrdersRelatedByBillingAddressIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Order objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Order objects.
     * @throws PropelException
     */
    public function countOrdersRelatedByBillingAddressId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collOrdersRelatedByBillingAddressIdPartial && !$this->isNew();
        if (null === $this->collOrdersRelatedByBillingAddressId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOrdersRelatedByBillingAddressId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOrdersRelatedByBillingAddressId());
            }

            $query = ChildOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBybillingAddress($this)
                ->count($con);
        }

        return count($this->collOrdersRelatedByBillingAddressId);
    }

    /**
     * Method called to associate a ChildOrder object to this object
     * through the ChildOrder foreign key attribute.
     *
     * @param  ChildOrder $l ChildOrder
     * @return $this|\TestShop\Models\UserAddress The current object (for fluent API support)
     */
    public function addOrderRelatedByBillingAddressId(ChildOrder $l)
    {
        if ($this->collOrdersRelatedByBillingAddressId === null) {
            $this->initOrdersRelatedByBillingAddressId();
            $this->collOrdersRelatedByBillingAddressIdPartial = true;
        }

        if (!$this->collOrdersRelatedByBillingAddressId->contains($l)) {
            $this->doAddOrderRelatedByBillingAddressId($l);

            if ($this->ordersRelatedByBillingAddressIdScheduledForDeletion and $this->ordersRelatedByBillingAddressIdScheduledForDeletion->contains($l)) {
                $this->ordersRelatedByBillingAddressIdScheduledForDeletion->remove($this->ordersRelatedByBillingAddressIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOrder $orderRelatedByBillingAddressId The ChildOrder object to add.
     */
    protected function doAddOrderRelatedByBillingAddressId(ChildOrder $orderRelatedByBillingAddressId)
    {
        $this->collOrdersRelatedByBillingAddressId[]= $orderRelatedByBillingAddressId;
        $orderRelatedByBillingAddressId->setbillingAddress($this);
    }

    /**
     * @param  ChildOrder $orderRelatedByBillingAddressId The ChildOrder object to remove.
     * @return $this|ChildUserAddress The current object (for fluent API support)
     */
    public function removeOrderRelatedByBillingAddressId(ChildOrder $orderRelatedByBillingAddressId)
    {
        if ($this->getOrdersRelatedByBillingAddressId()->contains($orderRelatedByBillingAddressId)) {
            $pos = $this->collOrdersRelatedByBillingAddressId->search($orderRelatedByBillingAddressId);
            $this->collOrdersRelatedByBillingAddressId->remove($pos);
            if (null === $this->ordersRelatedByBillingAddressIdScheduledForDeletion) {
                $this->ordersRelatedByBillingAddressIdScheduledForDeletion = clone $this->collOrdersRelatedByBillingAddressId;
                $this->ordersRelatedByBillingAddressIdScheduledForDeletion->clear();
            }
            $this->ordersRelatedByBillingAddressIdScheduledForDeletion[]= clone $orderRelatedByBillingAddressId;
            $orderRelatedByBillingAddressId->setbillingAddress(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserAddress is new, it will return
     * an empty collection; or if this UserAddress has previously
     * been saved, it will retrieve related OrdersRelatedByBillingAddressId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserAddress.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOrder[] List of ChildOrder objects
     */
    public function getOrdersRelatedByBillingAddressIdJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOrderQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getOrdersRelatedByBillingAddressId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserAddress is new, it will return
     * an empty collection; or if this UserAddress has previously
     * been saved, it will retrieve related OrdersRelatedByBillingAddressId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserAddress.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOrder[] List of ChildOrder objects
     */
    public function getOrdersRelatedByBillingAddressIdJoinCoupon(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOrderQuery::create(null, $criteria);
        $query->joinWith('Coupon', $joinBehavior);

        return $this->getOrdersRelatedByBillingAddressId($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aUser) {
            $this->aUser->removeUserAddress($this);
        }
        $this->id = null;
        $this->full_name = null;
        $this->address_line = null;
        $this->city = null;
        $this->province = null;
        $this->country = null;
        $this->zip_code = null;
        $this->telephone = null;
        $this->user_id = null;
        $this->status = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collOrdersRelatedByAddressId) {
                foreach ($this->collOrdersRelatedByAddressId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collOrdersRelatedByBillingAddressId) {
                foreach ($this->collOrdersRelatedByBillingAddressId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collOrdersRelatedByAddressId = null;
        $this->collOrdersRelatedByBillingAddressId = null;
        $this->aUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserAddressTableMap::DEFAULT_STRING_FORMAT);
    }

    // validate behavior

    /**
     * Configure validators constraints. The Validator object uses this method
     * to perform object validation.
     *
     * @param ClassMetadata $metadata
     */
    static public function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('full_name', new NotBlank());
        $metadata->addPropertyConstraint('address_line', new NotBlank());
        $metadata->addPropertyConstraint('city', new NotBlank());
        $metadata->addPropertyConstraint('province', new NotBlank());
        $metadata->addPropertyConstraint('country', new NotBlank());
        $metadata->addPropertyConstraint('zip_code', new NotBlank());
        $metadata->addPropertyConstraint('telephone', new NotBlank());
        $metadata->addPropertyConstraint('user_id', new NotNull());
    }

    /**
     * Validates the object and all objects related to this table.
     *
     * @see        getValidationFailures()
     * @param      ValidatorInterface|null $validator A Validator class instance
     * @return     boolean Whether all objects pass validation.
     */
    public function validate(ValidatorInterface $validator = null)
    {
        if (null === $validator) {
            $validator = new RecursiveValidator(
                new ExecutionContextFactory(new IdentityTranslator()),
                new LazyLoadingMetadataFactory(new StaticMethodLoader()),
                new ConstraintValidatorFactory()
            );
        }

        $failureMap = new ConstraintViolationList();

        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            // If validate() method exists, the validate-behavior is configured for related object
            if (method_exists($this->aUser, 'validate')) {
                if (!$this->aUser->validate($validator)) {
                    $failureMap->addAll($this->aUser->getValidationFailures());
                }
            }

            $retval = $validator->validate($this);
            if (count($retval) > 0) {
                $failureMap->addAll($retval);
            }

            if (null !== $this->collOrdersRelatedByAddressId) {
                foreach ($this->collOrdersRelatedByAddressId as $referrerFK) {
                    if (method_exists($referrerFK, 'validate')) {
                        if (!$referrerFK->validate($validator)) {
                            $failureMap->addAll($referrerFK->getValidationFailures());
                        }
                    }
                }
            }
            if (null !== $this->collOrdersRelatedByBillingAddressId) {
                foreach ($this->collOrdersRelatedByBillingAddressId as $referrerFK) {
                    if (method_exists($referrerFK, 'validate')) {
                        if (!$referrerFK->validate($validator)) {
                            $failureMap->addAll($referrerFK->getValidationFailures());
                        }
                    }
                }
            }

            $this->alreadyInValidation = false;
        }

        $this->validationFailures = $failureMap;

        return (Boolean) (!(count($this->validationFailures) > 0));

    }

    /**
     * Gets any ConstraintViolation objects that resulted from last call to validate().
     *
     *
     * @return     object ConstraintViolationList
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
