<?php

namespace TestShop\Models\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use TestShop\Models\UserAddress;
use TestShop\Models\UserAddressQuery;


/**
 * This class defines the structure of the 'address' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserAddressTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'TestShop.Models.Map.UserAddressTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'address';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\TestShop\\Models\\UserAddress';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'TestShop.Models.UserAddress';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'address.id';

    /**
     * the column name for the full_name field
     */
    const COL_FULL_NAME = 'address.full_name';

    /**
     * the column name for the address_line field
     */
    const COL_ADDRESS_LINE = 'address.address_line';

    /**
     * the column name for the city field
     */
    const COL_CITY = 'address.city';

    /**
     * the column name for the province field
     */
    const COL_PROVINCE = 'address.province';

    /**
     * the column name for the country field
     */
    const COL_COUNTRY = 'address.country';

    /**
     * the column name for the zip_code field
     */
    const COL_ZIP_CODE = 'address.zip_code';

    /**
     * the column name for the telephone field
     */
    const COL_TELEPHONE = 'address.telephone';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'address.user_id';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'address.status';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'FullName', 'AddressLine', 'City', 'Province', 'Country', 'ZipCode', 'Telephone', 'UserId', 'Status', ),
        self::TYPE_CAMELNAME     => array('id', 'fullName', 'addressLine', 'city', 'province', 'country', 'zipCode', 'telephone', 'userId', 'status', ),
        self::TYPE_COLNAME       => array(UserAddressTableMap::COL_ID, UserAddressTableMap::COL_FULL_NAME, UserAddressTableMap::COL_ADDRESS_LINE, UserAddressTableMap::COL_CITY, UserAddressTableMap::COL_PROVINCE, UserAddressTableMap::COL_COUNTRY, UserAddressTableMap::COL_ZIP_CODE, UserAddressTableMap::COL_TELEPHONE, UserAddressTableMap::COL_USER_ID, UserAddressTableMap::COL_STATUS, ),
        self::TYPE_FIELDNAME     => array('id', 'full_name', 'address_line', 'city', 'province', 'country', 'zip_code', 'telephone', 'user_id', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'FullName' => 1, 'AddressLine' => 2, 'City' => 3, 'Province' => 4, 'Country' => 5, 'ZipCode' => 6, 'Telephone' => 7, 'UserId' => 8, 'Status' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'fullName' => 1, 'addressLine' => 2, 'city' => 3, 'province' => 4, 'country' => 5, 'zipCode' => 6, 'telephone' => 7, 'userId' => 8, 'status' => 9, ),
        self::TYPE_COLNAME       => array(UserAddressTableMap::COL_ID => 0, UserAddressTableMap::COL_FULL_NAME => 1, UserAddressTableMap::COL_ADDRESS_LINE => 2, UserAddressTableMap::COL_CITY => 3, UserAddressTableMap::COL_PROVINCE => 4, UserAddressTableMap::COL_COUNTRY => 5, UserAddressTableMap::COL_ZIP_CODE => 6, UserAddressTableMap::COL_TELEPHONE => 7, UserAddressTableMap::COL_USER_ID => 8, UserAddressTableMap::COL_STATUS => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'full_name' => 1, 'address_line' => 2, 'city' => 3, 'province' => 4, 'country' => 5, 'zip_code' => 6, 'telephone' => 7, 'user_id' => 8, 'status' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('address');
        $this->setPhpName('UserAddress');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\TestShop\\Models\\UserAddress');
        $this->setPackage('TestShop.Models');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('full_name', 'FullName', 'VARCHAR', true, 255, null);
        $this->addColumn('address_line', 'AddressLine', 'VARCHAR', true, 255, null);
        $this->addColumn('city', 'City', 'VARCHAR', true, 255, null);
        $this->addColumn('province', 'Province', 'VARCHAR', true, 255, null);
        $this->addColumn('country', 'Country', 'VARCHAR', true, 255, null);
        $this->addColumn('zip_code', 'ZipCode', 'VARCHAR', true, 15, null);
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', true, 20, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user', 'id', true, null, null);
        $this->addColumn('status', 'Status', 'INTEGER', true, null, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', '\\TestShop\\Models\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('OrderRelatedByAddressId', '\\TestShop\\Models\\Order', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':address_id',
    1 => ':id',
  ),
), null, null, 'OrdersRelatedByAddressId', false);
        $this->addRelation('OrderRelatedByBillingAddressId', '\\TestShop\\Models\\Order', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':billing_address_id',
    1 => ':id',
  ),
), null, null, 'OrdersRelatedByBillingAddressId', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'validate' => array('rule1' => array ('column' => 'full_name','validator' => 'NotBlank',), 'rule2' => array ('column' => 'address_line','validator' => 'NotBlank',), 'rule3' => array ('column' => 'city','validator' => 'NotBlank',), 'rule4' => array ('column' => 'province','validator' => 'NotBlank',), 'rule5' => array ('column' => 'country','validator' => 'NotBlank',), 'rule6' => array ('column' => 'zip_code','validator' => 'NotBlank',), 'rule7' => array ('column' => 'telephone','validator' => 'NotBlank',), 'rule8' => array ('column' => 'user_id','validator' => 'NotNull',), ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserAddressTableMap::CLASS_DEFAULT : UserAddressTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UserAddress object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserAddressTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserAddressTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserAddressTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserAddressTableMap::OM_CLASS;
            /** @var UserAddress $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserAddressTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserAddressTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserAddressTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UserAddress $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserAddressTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserAddressTableMap::COL_ID);
            $criteria->addSelectColumn(UserAddressTableMap::COL_FULL_NAME);
            $criteria->addSelectColumn(UserAddressTableMap::COL_ADDRESS_LINE);
            $criteria->addSelectColumn(UserAddressTableMap::COL_CITY);
            $criteria->addSelectColumn(UserAddressTableMap::COL_PROVINCE);
            $criteria->addSelectColumn(UserAddressTableMap::COL_COUNTRY);
            $criteria->addSelectColumn(UserAddressTableMap::COL_ZIP_CODE);
            $criteria->addSelectColumn(UserAddressTableMap::COL_TELEPHONE);
            $criteria->addSelectColumn(UserAddressTableMap::COL_USER_ID);
            $criteria->addSelectColumn(UserAddressTableMap::COL_STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.full_name');
            $criteria->addSelectColumn($alias . '.address_line');
            $criteria->addSelectColumn($alias . '.city');
            $criteria->addSelectColumn($alias . '.province');
            $criteria->addSelectColumn($alias . '.country');
            $criteria->addSelectColumn($alias . '.zip_code');
            $criteria->addSelectColumn($alias . '.telephone');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.status');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserAddressTableMap::DATABASE_NAME)->getTable(UserAddressTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserAddressTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserAddressTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserAddressTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UserAddress or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UserAddress object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAddressTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \TestShop\Models\UserAddress) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserAddressTableMap::DATABASE_NAME);
            $criteria->add(UserAddressTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UserAddressQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserAddressTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserAddressTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the address table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserAddressQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UserAddress or Criteria object.
     *
     * @param mixed               $criteria Criteria or UserAddress object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAddressTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UserAddress object
        }

        if ($criteria->containsKey(UserAddressTableMap::COL_ID) && $criteria->keyContainsValue(UserAddressTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserAddressTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UserAddressQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserAddressTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserAddressTableMap::buildTableMap();
