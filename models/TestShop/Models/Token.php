<?php

namespace TestShop\Models;

use TestShop\Models\Base\Token as BaseToken;

/**
 * Skeleton subclass for representing a row from the 'token' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Token extends BaseToken
{
  public function __construct(){
    parent::__construct();

    if($this->isNew()){
      $this->setCode(md5("TestShop-imaginate".time()."-".microtime()));
      $this->setValidUntil(date("Y-m-d H:i:s", (time()+2678400)));
    }
  }
  public function toObject(){
    $ret = new \StdClass();
    $ret->valid_until = $this->getValidUntil()->format(\DateTime::ISO8601);
    $ret->token = $this->getCode();
    return $ret;
  }

}
