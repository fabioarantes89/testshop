<?php

namespace TestShop\Models;
include_once("lib/Utils.php");

use TestShop\Models\Base\Product as BaseProduct;
use \Propel\Runtime\Connection as Connection;
/**
 * Skeleton subclass for representing a row from the 'product' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Product extends BaseProduct
{
  protected $simulateDelete = false;
  public function delete(Connection\ConnectionInterface $con = NULL){
    $this->simulateDelete = true;
    $this->setStatus(0);
    $this->save();
    $this->simulateDelete = false;
  }
  public function isDeleted(Connection\ConnectionInterface $con = NULL){
    if($this->simulateDelete){
        return parent::isDeleted($con);
    }
    if($this->getStatus() == 0){
      return true;
    } else {
      return false;
    }
  }
  public function getPriceInCurrency(String $currency){
      $currentCurrency = $this->getCurrency();
      return \TestShop\Utils::convertCurrency($currentCurrency, $this->getPrice(), $currency);
  }

  public function formatCurrency(String $currency = null, Float $price = null){
    $currency = $currency ?? $this->getCurrency();
    $price = $price ?? $this->getPriceInCurrency($currency);

    return \TestShop\Utils::formatCurrency($currency, $price);
  }
  public function toJSON($limited = true){
    $fields = json_decode(parent::toJSON());
    if($limited){
      unset($fields->Text);
    }
    unset($fields->Currency);
    unset($fields->Status);
    $fields->Price = $this->getPriceInCurrency("USD");
    return $fields;
  }

}
