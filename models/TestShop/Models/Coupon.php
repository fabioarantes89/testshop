<?php

namespace TestShop\Models;
use TestShop\Models\Base\Coupon as BaseCoupon;
use \Propel\Runtime\Connection as Connection;
/**
 * Skeleton subclass for representing a row from the 'coupon' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Coupon extends BaseCoupon
{
  protected $simulateDelete = false;
  public function delete(Connection\ConnectionInterface $con = NULL){
    $this->simulateDelete = true;
    $this->setStatus(0);
    $this->save();
    $this->simulateDelete = false;
  }
  public function isDeleted(Connection\ConnectionInterface $con = NULL){
    if($this->simulateDelete){
        return parent::isDeleted($con);
    }
    if($this->getStatus() == 0){
      return true;
    } else {
      return false;
    }
  }
  public function __construct(){
    parent::__construct();

    if($this->isNew()){
      $this->setCode($this->generateCode());
    }
  }
  public function generateCode(){
    $code = md5("TestShop".date("mdY:is").time());
    preg_match_all("/([a-z0-9]{5})/i", $code, $matches);
    return implode("-", $matches[0]);
  }
  public function alreadyUsed(){
    $orders = $this->getOrders()->count();
    return !empty($orders);
  }
  public function getCouponValue($currency){

    switch($this->getType()){
      case "PERCENT":
        return (($this->getDiscount() / 100));
      break;
      case "PRICE":
        return ($this->getPriceInCurrency($currency));
      break;
    }
    return;
  }
  public function getHumamReadable($currency){
    switch($this->getType()){
      case "PERCENT":
        return ($this->getCouponValue($currency)*100) . "%";
      break;
      case "PRICE":
        return (\TestShop\Utils::formatCurrency($currency, $this->getCouponValue($currency)));
      break;
    }
    return;
  }
  public function getPriceInCurrency(String $currency){
    $currentCurrency = $this->getCurrency();
    return \TestShop\Utils::convertCurrency($currentCurrency, $this->getDiscount(), $currency);
  }
  public function toJSON($limited = true){
    $fields = json_decode(parent::toJSON());
    if($fields->Type == "PRICE"){
      $fields->Discount = $this->getPriceInCurrency("USD");
    }
    return $fields;
  }
}
