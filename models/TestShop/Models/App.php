<?php

namespace TestShop\Models;

use TestShop\Models\Base\App as BaseApp;
use TestShop\Models\Base\Coupon as BaseCoupon;
use \Propel\Runtime\Connection as Connection;
/**
 * Skeleton subclass for representing a row from the 'app' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class App extends BaseApp
{
  protected $simulateDelete = false;
  public function delete(Connection\ConnectionInterface $con = NULL){
    $this->simulateDelete = true;
    $this->setStatus(0);
    $this->save();
    $this->simulateDelete = false;
  }
  public function isDeleted(Connection\ConnectionInterface $con = NULL){
    if($this->simulateDelete){
        return parent::isDeleted($con);
    }
    if($this->getStatus() == 0){
      return true;
    } else {
      return false;
    }
  }
  public function __construct(){
    parent::__construct();

    if($this->isNew()){
      $this->setSecret($this->generateCode());
      $this->setAppKey($this->generateCode());
    }

  }
  public function generateCode(){
    $code = md5("TestShop".date("mdY:is").time().microtime());
    return $code;
  }
}
