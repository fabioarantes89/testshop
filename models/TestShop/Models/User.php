<?php

namespace TestShop\Models;

use TestShop\Models\Base\User as BaseUser;
use \Propel\Runtime\Connection as Connection;
/**
 * Skeleton subclass for representing a row from the 'user' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User extends BaseUser
{
  protected $simulateDelete = false;
  public function delete(Connection\ConnectionInterface $con = NULL){
    $this->simulateDelete = true;
    $this->setStatus(0);
    $this->save();
    $this->simulateDelete = false;
  }
  public function isDeleted(Connection\ConnectionInterface $con = NULL){
    if($this->simulateDelete){
        return parent::isDeleted($con);
    }
    if($this->getStatus() == 0){
      return true;
    } else {
      return false;
    }
  }

  public function preInsert(Connection\ConnectionInterface $con = NULL)
  {
    if($this->isColumnModified('user.salt')){
      $this->setSalt(md5($this->getSalt()));
    }
    return true;
  }
  public function preUpdate(Connection\ConnectionInterface $con = NULL)
  {

    if($this->isColumnModified('user.salt')){
      $this->setSalt(md5($this->getSalt()));
    }
    return true;
  }
  public function toJSON(){
    $fields = json_decode(parent::toJSON());
    unset($fields->Salt);
    unset($fields->Type);
    unset($fields->Status);
    if(!empty($fields->Tokens)){
      unset($fields->Tokens);
    }
    return $fields;
  }

}
