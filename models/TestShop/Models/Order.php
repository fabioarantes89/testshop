<?php

namespace TestShop\Models;
require_once("lib/Utils.php");
use TestShop\Models\Base\Order as BaseOrder;

/**
 * Skeleton subclass for representing a row from the 'order' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Order extends BaseOrder
{
  public function setStatus($status){
      if(is_int($status)){
        return parent::setStatus($status);
      }
      $step = $this->getStepInt($status);
      return parent::setStatus($step);
  }
  public function getStepInt($stepName){
    foreach($this->getSteps() as $i => $step){
      if($stepName == \TestShop\Utils::Slugfy($step)){
        return $i;
      }
    }
    return 0;
  }
  public function getSubTotal(String $currency){
    $lists = $this->getOrderLists();
    $price = 0;
    foreach($lists as $list){
      $price += $list->getProduct()->getPriceInCurrency($currency) * $list->getQuantity();
    }
    return $price;
  }

  public function getTotal(String $currency){
    $sub = $this->getSubTotal($currency);
    $coupon = $this->getCoupon();
    $total = $sub;
    if($coupon){
      switch($coupon->getType()){
        case "PERCENT":
          $total = $sub * (1 - ($coupon->getDiscount() / 100));
        break;
        case "PRICE":
          $total = $sub - ($coupon->getPriceInCurrency($currency));
        break;
      }
    }

    return $total;
  }
  public function getSteps(){
    return array("Order Accept",
    "On Hold",
    "Approved Payment",
    "Order Shipped",
    "Closed");
  }
  public function toJSON($limited = true){
    $fields = json_decode(parent::toJSON());
    $fields->Total = $this->getTotal("USD");
    $fields->Status = $this->getSteps()[$this->getStatus()];
    if(!empty($fields->OrderLists)){
      for($i=0; $i<count($fields->OrderLists); $i++){
        $fields->OrderLists[$i]->Product = ProductQuery::create()->findPk($fields->OrderLists[$i]->ProductId)->toJSON();
      }
    }
    return $fields;
  }
}
