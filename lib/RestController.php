<?php
namespace TestShop\Controllers;
require_once("DefaultController.php");
use \TestShop\Models as Models;


class RestController extends DefaultController {
  public function getAuthorization(){
    $ret = new \StdClass();
    $ret->status = "ERROR";
    $headers = apache_request_headers();
    if(!empty($headers['Authorization'])){
      preg_match('/(Bearer: |bearer )?(.+)/i', $headers['Authorization'], $matches);
      $token = $matches[2];




      $token = Models\TokenQuery::create()->joinWith("App")->joinWith("User")->findByCode($token);

      if($token->count() < 1){
        $ret->message = "Invalid Token";
        return $this->renderError("Invalid Token", 403);
      } else {
        $this->token = $token[0];
        $this->user = $this->token->getUser();
        $this->App = $this->token->getApp();

      }
    } else {
      return $this->renderError("Missing Authorization", 403);
    }
  }
  public function renderError($message, $status = 500){
    $ret = new \StdClass();
    $ret->status = "ERROR";
    $ret->message = $message;
    $this->renderJSON($ret, $status);
  }
}
?>
