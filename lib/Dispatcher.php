<?php
namespace TestShop;
use TestShop\Routes as Routes;
include_once("lib/ErrorController.php");
require_once("RouteAction.php");
require_once("SecureRouteAction.php");
require_once("RouteException.php");

class Dispatcher {
  static private $dispatcher;
  static $routes = array();
  static $session;
  public $activeRoute;
  static function init(){
      if(!empty(self::$dispatcher)){
        return self::$dispatcher;
      }
      self::$dispatcher = new Dispatcher();
      return self::$dispatcher;
  }
  public static function getAppPrefix(){
    return Application::$app->getConfig(APP_WEB_PREFIX);
  }
  private function prepareURI(){

    return str_replace($this->getAppPrefix(), "", $_SERVER['REQUEST_URI']);
  }
  public function parseRoute(): Bool{
    foreach(Dispatcher::$routes as $i => $route){

      if($route->acceptRoute($this->prepareURI())){
        $this->activeRoute = $route;
        unset(Dispatcher::$routes[$i]);

        return true;
      }
    }

    throw new Routes\RouteException("404 Page Not Found", 1);
    return false;
  }

  public function callController(Application $app){
    if($this->activeRoute){
      try {
        $controller = $app->getController($this->activeRoute->getController(), $this->activeRoute);
        $action = $this->activeRoute->getAction();
        if(!$action || !method_exists($controller, $action)){
          throw new Routes\RouteException("Method Invalid");
        }
        $controller->$action($this->activeRoute);

      } catch(\Exception $e){
        var_dump($e);
        $this->callError();
      }
    } else {
      $this->callError();
    }
  }
  public function callError(){
    $controller = new Controllers\ErrorController();
    $controller->index();
  }
  static function startSession(){
    if(empty(self::$session)){
      session_start();
      self::$session = true;
    }
  }
  static function setSessionValue($name, $val){
    self::startSession();
    $_SESSION[$name] = $val;
  }
  static function getSessionValue($name){
    self::startSession();
    return $_SESSION[$name];
  }
  static function hasSessionValue($name){
    self::startSession();
    return !empty($_SESSION[$name]);
  }
  static function isUserLogged(){
    self::startSession();
    return !empty($_SESSION['userData']);
  }
  static function setUser($userInfo){
    self::startSession();
    $_SESSION['userData'] = $userInfo;
  }
  static function getUser(){
    self::startSession();
    return $_SESSION['userData'];
  }
  static function unsetUser($userInfo){
    self::startSession();
    unset($_SESSION['userData']);
  }
  static function unsetSessionValue($name){
    self::startSession();
    unset($_SESSION[$name]);
  }
  static function addRoute(Routes\RouteAction $action){
    self::$routes[] = $action;
  }

  public function nextRoute(){
    unset($this->activeRoute);
    $this->parseRoute();
    $this->callController(Application::$app);
  }
  static function redirect($rawURL){
    $prefix = self::getAppPrefix();
    $rawURL = (substr($rawURL, 0, strlen($prefix)) != $prefix)?$prefix.$rawURL:$rawURL;

    header("Location: http://{$_SERVER['HTTP_HOST']}".$rawURL);
  }
}
?>
