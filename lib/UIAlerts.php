<?php
  namespace TestShop;

  class UIAlerts {
    public $text, $type;
    private $acceptedTypes = array("success", "error");
    private $classes = array("success" => "alert-success", "error" => "alert-danger");
    public function __construct(String $type, String $text) {
      if(!in_array($type, $this->acceptedTypes) ){
        throw new \Exception("Invalid UI Type", sprintf("Received: %s, valid Types: %s", $type, implode(", ", $this->acceptedTypes)));
      }
      $this->type = $type;
      $this->text = $text;
    }
    public function getAlertClass(): String{
      if(!empty($this->classes[$this->type])){
        return $this->classes[$this->type];
      }
      return "alert-info";
    }
  }
?>
