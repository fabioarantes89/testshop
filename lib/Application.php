<?php
namespace TestShop;
require_once("Dispatcher.php");
define("UPLOADS_WEB_PREFIX", "uploads_web_prefix");
define("UPLOADS_DIR", "uploads_dir");
define("UPLOADS_PATH_PREFIX", "upload_path_prefix");
define("APP_AUTH_URL", "app_auth_url");
define("IS_PRIVATE_APP", "is_private_app");
define("DEFAULT_RESPONSE_FORMAT", "default_response_format");
define("APP_WEB_PREFIX", "app_web_prefix");


use TestShop;
use TestShop\Routes;

abstract class Application {
  static $dispatcher;
  private $appName;
  static $app;
  protected $configs = array();

  public function __construct(String $appName){
    $this->appName = $appName;
    self::$dispatcher = Dispatcher::init();
    $this->configure();
  }
  protected function configure(){
    $this->setConfig(IS_PRIVATE_APP, false);
    $this->setConfig(APP_WEB_PREFIX, "/");
    $this->setConfig(DEFAULT_RESPONSE_FORMAT, "html");
    $this->loadRoutes();
  }
  public function getName(){
    return $this->appName;
  }
  protected function loadRoutes(){
    require_once("apps/{$this->appName}/config/routes.php");
  }
  public function startRoute(){
    //if(($this->getConfig(IS_PRIVATE_APP) && !Dispatcher::isUserLogged()) && $_SERVER['REQUEST_URI'] != $this->getConfig(APP_AUTH_URL)){
    //  Dispatcher::redirect($this->getConfig(APP_AUTH_URL));
    //}
    try {
      try {
        if(self::$dispatcher->parseRoute()){
          self::$dispatcher->callController($this);
        }
      } catch(Routes\SecurityException $e){
        $this->securityException($e);
      }
    } catch(Routes\RouteException $e){

      self::$dispatcher->callError();
    }

  }
  protected function securityException(Routes\SecurityException $e){
    Dispatcher::setSessionValue("message_error", "You must be logged");
    Dispatcher::redirect($this->getConfig(APP_AUTH_URL));
  }
  public function setConfig(String $configName, $configValue){
    $this->configs[$configName] = $configValue;
  }
  public function getConfig(String $configName){
    if(!empty($this->configs[$configName])){
      return $this->configs[$configName];
    }
    return false;
  }
  public function uploadFile(String $fieldName){
    if(empty($_FILES[$fieldName]) || $_FILES[$fieldName]['error']){ return false; }
    if(!$this->getConfig(UPLOADS_WEB_PREFIX) || !$this->getConfig(UPLOADS_DIR) || !$this->getConfig(UPLOADS_PATH_PREFIX)){ throw new \Exception("Uploads config missing in app"); }

    $file = $_FILES[$fieldName];
    $fileName = $file['name'];
    preg_match("/(.+)\.([a-z0-9]{3}|[a-z0-9]{4})$/i", $fileName, $matches);
    $ext = $matches[2];
    $name = $matches[1];

    $path = $this->generateUploadDirPath();

    $nFileName = implode(".", array($name, time(), $ext));
    $nPath = $path . $nFileName;

    if(!move_uploaded_file($file['tmp_name'], $nPath)){
      throw new \Exception(sprintf("Failed to move uploaded file in Path: %s", $nPath));
    }

    return str_replace(DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, (str_replace($this->getConfig(UPLOADS_PATH_PREFIX), $this->getConfig(UPLOADS_WEB_PREFIX), $nPath)));
  }
  private function generateUploadDirPath(){
    $path = str_replace(DIRECTORY_SEPARATOR."".DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, array($this->getConfig(UPLOADS_PATH_PREFIX), $this->getConfig(UPLOADS_DIR), date("Y"), date("m"), date("d"))));
    return \TestShop\Utils::createPath($path);
  }
  public function deleteFile(String $path){
    if(!$this->getConfig(UPLOADS_WEB_PREFIX) || !$this->getConfig(UPLOADS_DIR) || !$this->getConfig(UPLOADS_PATH_PREFIX)){ throw new \Exception("Uploads config missing in app"); }
    $pathPrefix = $this->getConfig(UPLOADS_PATH_PREFIX);
    $urlPrefix = str_replace("/", "\/", $this->getConfig(UPLOADS_WEB_PREFIX));
    $path = preg_replace("/^{$urlPrefix}(.+)/", "{$pathPrefix}$1", $path);

    if(file_exists($path) && is_file($path)){
      return unlink($path);
    }
  }
}

?>
