<?php
  namespace TestShop\Views;
  use TestShop as TS;

  final class ViewPart {
    private $path;

    public function __construct($path){
      $this->path = $path;
    }
    public function includePartial(String $module, String $layout){
      $app = TS\Application::$app->getName();
      $path = "apps/{$app}/controllers/{$module}/views/_{$layout}.php";

      if(file_exists($path)){
        include($path);
      } else {
        throw new LayoutException("Partial not Found");
      }

    }
    public function render(): String {
      $data = "";
      if(file_exists($this->path)){
          ob_start();
          include $this->path;
          $data = ob_get_contents();
          ob_end_clean();
      }
      return $data;
    }
    public function Slugfy($term){
      return \TestShop\Utils::Slugfy($term);
    }
    public function formatYesNo($value){
      return ($value)?"YES":"NO";
    }
    public function generateURL($url){
      $prefix = \TestShop\Dispatcher::getAppPrefix();
      return $prefix.$url;
    }
    public function formatCurrency($currencyFormat, $value){
      return \TestShop\Utils::formatCurrency($currencyFormat, $value);
    }
  }
?>
