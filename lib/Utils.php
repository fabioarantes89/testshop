<?php
namespace TestShop;

final class Utils {
    static $validCurrencies = array("EUR", "USD", "BRL");
    static $currencyValues = array(
      "USD" => array("BRL" => 3.38, "EUR" => 0.90),
      "BRL" => array("USD" => 0.30, "EUR" => 0.27),
      "EUR" => array("USD" => 1.11, "BRL" => 3.74),
    );
    static $currencyFormats = array(
      "USD" => array("SYMBOL" => "US$", "DECIMAL" => ".", "THOUSAND" => ","),
      "BRL" => array("SYMBOL" => "R$", "DECIMAL" => ",", "THOUSAND" => "."),
      "EUR" => array("SYMBOL" => "€", "DECIMAL" => ",", "THOUSAND" => "."),
    );

    static function checkCurrency(String $currency){
      if(!in_array($currency, self::$validCurrencies)){
        throw new \Exception("Currency not supported");
      }
    }
    static function convertCurrency(String $currency, Float $price, String $toCurrency){
      self::checkCurrency($currency);
      self::checkCurrency($toCurrency);

      if($currency == $toCurrency){
        return $price;
      }
      $newPrice = $price * self::$currencyValues[$currency][$toCurrency];
      return $newPrice;
    }
    static function formatCurrency(String $currency, Float $price):String {
      self::checkCurrency($currency);

      $price = number_format($price, 2, self::$currencyFormats[$currency]['DECIMAL'], self::$currencyFormats[$currency]['THOUSAND']);
      return sprintf("%s%s", self::$currencyFormats[$currency]['SYMBOL'], $price);
    }

    static function Slugfy($term){
      return strtolower(preg_replace('/[^a-zA-Z0-9]/i', '-', $term));
    }
    static function createPath(String $path){
      $Dir = explode(DIRECTORY_SEPARATOR, $path);

      $nPath = "";


      foreach($Dir as $dir){
        $nPath = $nPath.$dir.DIRECTORY_SEPARATOR;
        if(!self::fileExists($nPath)){
          if(!self::createDir($nPath)){
            throw new \Exception(sprintf("Failed to create Path %s", $nPath));
          }
        }
      }
      return $nPath;
    }

    static function fileExists(String $path){
      return file_exists($path);
    }
    static function createDir($dir, Int $mode = 0700){

      return mkdir($dir, $mode);
    }
}

?>
