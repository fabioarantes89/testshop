<?php
namespace TestShop\Routes;
require_once("SecurityException.php");
use TestShop;

class SecureRouteAction extends RouteAction {
  public function acceptRoute(String $route): Bool {

    $ret = preg_match($this->PREG, $route);
    if($ret && $this->checkMethod()){
        if($this->activateRoute($route))
        {
          if(!TestShop\Dispatcher::isUserLogged()){
            throw new SecurityException("Error Processing Request", 1);
          }
        }
    }
    return $ret;
  }
}
?>
