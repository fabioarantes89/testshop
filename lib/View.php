<?php
  namespace TestShop\Views;
  use TestShop\Controllers as TC;
  use TestShop as TS;
  include_once("LayoutException.php");
  include_once("ViewPart.php");

  class View {
    private $data;
    private $layoutData, $controller, $layout,  $format, $template, $module, $appName;
    public function __construct(String $layout, String $module, String $view, TC\DefaultController $controller, String $format = "html"){
      $this->appName = TS\Application::$app->getName();
      $this->controller = $controller;
      $this->format = $format;
      $this->template = $view;
      $this->module = $module;
      $this->layout = $layout;

      if(!empty($controller->layoutData)){
        $this->layoutData = $controller->layoutData;
      }

    }
    public function renderContent():String {
      $path = "apps/{$this->appName}/controllers/{$this->module}/views/{$this->template}Success.{$this->format}.php";

      if(file_exists($path)){
          $layout = new ViewPart($path);
          foreach($this->controller as $key => $value){
            $layout->$key = $value;
          }

          return $layout->render();
      } else {
        throw new LayoutException(sprintf("Missing template in location: %s", $path));
      }
      return "";
    }
    public function render():String {

      $content = $this->renderContent();
      if($this->format != "html"){
        return $content;
      }

      $path = "apps/{$this->appName}/layouts/{$this->layout}.php";
      if(file_exists($path)){
          $layout = new ViewPart($path);
          $layout->content = $content;

          if(!empty($this->layoutData)){
            if(is_array($this->layoutData) || is_object($this->layoutData)){
              foreach($this->layoutData as $key => $value){
                $layout->$key = $value;
              }
            } else {
              $layout->data = $this->layoutData;
            }
          }
          return $layout->render();
      } else {
        throw new LayoutException("Layout not Found");
      }
    }
  }
?>
