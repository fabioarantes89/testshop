<?php
namespace TestShop\Routes;

class RouteAction {
  public $PREG, $controller, $action, $vars, $method;
  private $calledController, $calledAction, $calledVars;
  public function __construct(String $preg, String $controller, String $action, Array $vars = null, String $method = "ALL"){
    $this->PREG = $preg;
    $this->controller = $controller;
    $this->action = $action;
    $this->vars = $vars;
    $this->method = strtoupper($method);
  }

  protected function checkMethod(): Bool{
    if($this->method == "ALL"){
      return true;
    }
    return ($_SERVER['REQUEST_METHOD'] == $this->method);
  }
  public function acceptRoute(String $route): Bool {
    $ret = preg_match($this->PREG, $route);
    if($ret && $this->checkMethod()){
        return $this->activateRoute($route);
    }
    return ($ret && $this->checkMethod());
  }
  protected function isSpecialName(String $param): Bool {
      return preg_match("/:([0-9]+)/i", $param);
  }
  protected function getSpecialName(String $param, String $value): String {
      preg_match("/:([0-9]+)/i", $param, $match);
      $index = $match[1];
      preg_match($this->PREG, $value, $matches);
      return $matches[$index];
  }
  protected function activateRoute($route):Bool {
      $this->calledController = (!$this->isSpecialName($this->controller))?$this->controller:$this->getSpecialName($this->controller, $route);
      $this->calledAction = (!$this->isSpecialName($this->action))?$this->action:$this->getSpecialName($this->action, $route);

      if(!empty($this->vars)){
          foreach($this->vars as $key => $val){
            $this->vars[$key] = (!$this->isSpecialName($val))?$val:$this->getSpecialName($val, $route);
          }
      }
      if($this->controller != $this->calledController) {
        return \TestShop\Application::$app->hasController($this->calledController, $this);
      }
      return true;
  }
  public function getController(){
    return $this->calledController;
  }
  public function getAction(){
    return $this->calledAction;
  }
  public function getVars()
  {
    return $this->vars;
  }
}

?>
