<?php
namespace TestShop\Controllers;
use Propel\Runtime\ActiveQuery\Criteria;
require_once("UIAlerts.php");
require_once("ContentException.php");
use TestShop\Routes as Routes;
use TestShop as TS;

abstract class DefaultController {
  protected $route;
  protected $layout = "default";

  public function __construct(Routes\RouteAction $route){
    $this->route = $route;

    if(TS\Application::$app->getConfig(IS_PRIVATE_APP)){
      $this->layoutData = new \StdClass();
      $this->layoutData->userLogged = TS\Dispatcher::getUser();
    }

    $this->checkAlerts();
  }
  protected function paginationVars(){
    $this->page = ($_GET['page']) ?? $_POST['page'] ?? 1;
    $this->perPage = ($_GET['per_page']) ?? $_POST['per_page'] ?? 10;
    $this->q = ($_GET['q'])?trim($_GET['q']):null;
    $this->getPaginationLink();
  }
  public function checkAlerts(){
    if(\TestShop\Dispatcher::hasSessionValue("message_success")){
      $this->message = new \TestShop\UIAlerts("success", \TestShop\Dispatcher::getSessionValue("message_success"));
      \TestShop\Dispatcher::unsetSessionValue("message_success");
    }
    if(\TestShop\Dispatcher::hasSessionValue("message_error")){
      $this->errors = array();
      $this->errors[] = new \TestShop\UIAlerts("error", \TestShop\Dispatcher::getSessionValue("message_error"));
      \TestShop\Dispatcher::unsetSessionValue("message_error");
    }
  }
  public function render(String $template, String $module = null){
    include_once("lib/View.php");
    $module = $module ?? $this->getControllerName();
    $format = $this->format ?? TS\Application::$app->getConfig(DEFAULT_RESPONSE_FORMAT);
    $view = new TS\Views\View($this->layout, $module, $template, $this, $format);
    echo $view->render();
  }
  public function getControllerName(){
    $cls = get_class($this);
    preg_match("/(.+)\/([a-z0-9]+)Controller$/i", str_replace("\\", "/", $cls), $matches);
    $cls = strtolower($matches[2]);
    return $cls;
  }
  public function setLayout(String $layout){
    $this->layout = $layout;
  }
  public function callNextRoute(){
    $dispatcher = TS\Dispatcher::init();
    $dispatcher->nextRoute();
  }
  public function Error404(){
    include_once("RouteException.php");

    throw new Routes\RouteException("Error Processing Request", 1);
  }
  public function getPaginationLink(){
    $this->paginationLink = preg_replace("/(\?|\&)page=([0-9]+)/i", "", $_SERVER['REQUEST_URI']);
		$this->paginationLink = (strpos($this->paginationLink, "?") === FALSE)?$this->paginationLink."?page=":$this->paginationLink."&page=";
  }
  protected function redirect($rawURL){
    TS\Dispatcher::redirect($rawURL);
  }
  protected function saveUpload(String $file_name){
    $path = \TestShop\Application::$app->uploadFile($file_name);
    return $path;
  }
  protected function defaultFilters($query){
    if(empty($_GET['show_deleted'])){
      $query->filterByStatus(0, Criteria::NOT_EQUAL);
    }
  }
  protected function renderJSON($ret, $status = 200){
    http_response_code($status);
    header("Content-type: application/JSON; charset=UTF-8");
    echo(json_encode($ret));
    exit(0);
  }

}
?>
