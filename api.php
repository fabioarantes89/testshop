<?php
use TestShop\Models as Models;
require("apps/api/app.php");
require_once("vendor/autoload.php");
require_once("generated-conf/config.php");

// Define a new APP Instance and set his name;
$app = API\Application::init();
$app->startRoute();
?>
