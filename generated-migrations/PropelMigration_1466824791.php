<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1466824791.
 * Generated on 2016-06-25 03:19:51 by fabioarantes
 */
class PropelMigration_1466824791
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here


        $sql = "INSERT INTO `address` (`full_name`, `address_line`, `city`, `province`, `country`, `zip_code`, `telephone`, `user_id`) VALUES ('Fábio Arantes', 'Rua Engargo Gengo, 14', 'São Paulo', 'SP', 'Brazil', '04456-090', '+5511974337676', 1)";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function preDown(MigrationManager $manager)
    {
      $sql = "DELETE FROM address WHERE full_name = 'Fabio Arantes' LIMIT 1";
      $pdo = $manager->getAdapterConnection('default');
      $stmt = $pdo->prepare($sql);
      $stmt->execute();
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '',
);
    }

}
