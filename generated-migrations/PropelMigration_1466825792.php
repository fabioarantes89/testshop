<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1466825792.
 * Generated on 2016-06-25 03:36:32 by fabioarantes
 */
class PropelMigration_1466825792
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
        $sql = "INSERT INTO `coupon` (`code`, `discount`, `currency`, `type`, `status`) VALUES ('YUIDKA-DATATSK-KASUUQW', 20, null, 'PERCENT', 1)";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
        $sql = "DELETE FROM `coupon` WHERE `code`= 'YUIDKA-DATATSK-KASUUQW'";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '',
);
    }

}
