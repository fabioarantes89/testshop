<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1466826323.
 * Generated on 2016-06-25 03:45:23 by fabioarantes
 */
class PropelMigration_1466826323
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
        $sql = "INSERT INTO `orders` (`user_id`, `date`, `address_id`, `billing_address_id`, `coupon_id`, `status`) VALUES (1, NOW(), 1, 2, 1, 0)";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
        $sql = "DELETE FROM `orders` WHERE `user_id` = 1 and `address_id` = 1 and `billing_address_id` = 2 and `coupon_id` = 1 LIMIT 1";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '',
);
    }

}
