<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1466965462.
 * Generated on 2016-06-26 18:24:22 by fabioarantes
 */
class PropelMigration_1466965462
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
        $sql = "INSERT INTO `app` (`name`, `secret`, `app_key`) VALUES ('TestShop Frond-end', MD5(CONCAT('imaginate-', NOW(), 'lkasjdlfkj')), MD5('imaginate-test-shop-key'))";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
        $sql = "DELETE FROM `app` WHERE name = 'TestShop Frond-end' LIMIT 1;";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '',
);
    }

}
