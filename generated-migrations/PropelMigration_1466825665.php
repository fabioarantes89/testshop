<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1466825665.
 * Generated on 2016-06-25 03:34:25 by fabioarantes
 */
class PropelMigration_1466825665
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
        $sql = "INSERT INTO `address` (`full_name`, `address_line`, `city`, `province`, `country`, `zip_code`, `telephone`, `user_id`) VALUES ('Fábio Arantes', 'Av. Eng. Luiz Carlos Berrini, 1748 - cj501', 'São Paulo', 'SP', 'Brazil', '04571-000', '+5511974337676', 1)";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
        $sql = "DELETE FROM address WHERE full_name = 'Fabio Arantes' and address_line = 'Av. Eng. Luiz Carlos Berrini, 1748 - cj501' LIMIT 1";
        $pdo = $manager->getAdapterConnection('default');
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '',
);
    }

}
