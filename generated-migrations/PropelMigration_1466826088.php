<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1466826088.
 * Generated on 2016-06-25 03:41:28 by fabioarantes
 */
class PropelMigration_1466826088
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
        $sql = "INSERT INTO `product` (`title`, `text`, `file`, `status`, `date`, `currency`, `price`) VALUES ('MacBook Pro 13\"', 'New MacBook', '/uploads/macbook.1237987123.jpg', 1, NOW(), 'EUR', 2000)";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
        $sql = "DELETE FROM  `product` WHERE title = 'MacBook Pro 13\"' LIMIT 1";
		    $pdo = $manager->getAdapterConnection('default');
		    $stmt = $pdo->prepare($sql);
		    $stmt->execute();
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '',
);
    }

}
